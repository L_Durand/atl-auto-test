# artosage

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Code couleur du site

La palette de couleurs : https://coolors.co/palette/264653-2a9d8f-e9c46a-f4a261-e76f51

Couleur principale : 0xFF2A9D8F (Turquoise)
Couleur secondaire : 0xFF264653 (Bleu Marine)
Couleur tertiaire : 0xFFE76F51 (Terre Rouge)
Bouton valider : 0xFF2A9D8F (Turquoise)
Bouton middle : 0xFFE9C46A (Jaune Pale)
Bouton rouge : 0xFFE76F51 (Terre Rouge)
Bouton info : 0xFF264653 (Bleu Marine)
