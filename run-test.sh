#!/bin/bash

flutter pub get

echo "Widget Tests"
flutter test test/widget_test.dart
echo "User Service Tests"
flutter test test/user_service_test.dart
echo "Post Service Tests"
flutter test test/post_service_test.dart