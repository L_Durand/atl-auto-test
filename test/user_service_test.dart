import 'package:flutter_test/flutter_test.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/models/storage_item.dart';
import 'package:artosage/service/storage_service.dart';
import 'package:flutter/cupertino.dart';

void main() {
  group('UserService', () {
    UserService userService = UserService();
    StorageService storageService = StorageService();

    setUp(() {
      userService = UserService();
      storageService = StorageService();
    });

    test('buildList should return a list of User objects', () {
      List<User> userList = userService.buildList();
      expect(userList, isA<List<User>>());
    });

    test('typeEnum should return the correct UserTypeEnum', () {
      int typeId = 1;
      UserTypeEnum userType = userService.typeEnum(typeId);
      expect(userType, equals(UserTypeEnum.civil));
    });


    test('formatUser should return a User object', () {
      dynamic activeUserData = {
        'id': 1,
        'first_name': 'John',
        'name': 'Doe',
        'email': 'john.doe@example.com',
        'location': {
          'latitude': 37.7749,
          'longitude': -122.4194,
        },
        'email_verified_at': '2022-01-01T00:00:00.000000Z',
        'updatedAt': '2022-01-01T00:00:00.000000Z',
        'created_at': '2022-01-01T00:00:00.000000Z',
      };
      User user = userService.formatUser(activeUserData);
      expect(user, isA<User>());
    });
  });
}