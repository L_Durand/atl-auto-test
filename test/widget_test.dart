import 'package:artosage/share/custom_input.dart';
import 'package:artosage/share/custom_password_input.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/views/civil_post_list.dart';
import 'package:artosage/views/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:artosage/main.dart';

void main() {
  group('login', () {
    testWidgets('Empty password', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Login()));
      await tester.enterText(find.byType(CustomInput), 'test');
      await tester.tap(find.byType(CustomValidationButton));
      await tester.pump();
      await tester.ensureVisible(
          find.text('Veuillez renseigner un mot de passe.'));
    });

    testWidgets('Empty email', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Login()));
      await tester.enterText(find.byType(CustomPasswordInput), 'test');
      await tester.tap(find.byType(CustomValidationButton));
      await tester.pump();
      await tester.ensureVisible(
          find.text('Veuillez renseigner une adresse e-mail.'));
    });

    testWidgets('Empty fields', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Login()));
      await tester.tap(find.byType(CustomValidationButton));
      await tester.pump();
      await tester.ensureVisible(
          find.text('Veuillez renseigner une adresse e-mail.'));
      await tester.ensureVisible(
          find.text('Veuillez renseigner un mot de passe.'));
    });

    testWidgets('Login success', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(home: Login()));
      await tester.enterText(find.byType(CustomInput), 'example@gmail.com');
      await tester.enterText(find.byType(CustomPasswordInput), 'password');
      await tester.tap(find.byType(CustomValidationButton));
    });
  });
}
