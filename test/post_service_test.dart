import 'package:artosage/models/comment.dart';
import 'package:artosage/share/custom_popup.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';

import 'package:artosage/service/post_service.dart';

void main() {
  group('PostService', () {
    late PostService postService = PostService();

    test('getPosts should return a list of posts', () async {
      final expectedPosts = [
        HardData.allPosts[0],
        HardData.allPosts[1],
      ];

      final result = await postService.getPosts(test: true);

      expect(result, equals(expectedPosts));
    });

    test('getAllPosts should return a list of posts filtered by type', () async {
      final token = 'token';
      final type = [PostTypeEnum.ad];
      final users = [
        HardData.user1,
        HardData.user2,
      ];
      final expectedPosts = HardData.allPosts;

      final result = await postService.getAllPosts(token, type, users, test: true);

      expect(result, equals(expectedPosts));
    });

    test('saveNewPost should save a new post', () async {
      final context = null;
      final token = 'token';
      final idUser = '1';
      final postType = '1';
      final description = 'New post';
      final selectedFiles = [];

      await postService.saveNewPost(
          context, token, idUser, postType, description, selectedFiles, test: true);
    });

    test('addComment should add a new comment', () async {
      final content = 'New comment';
      final post = '1';
      final token = 'token';
      final context = null;

      await postService.addComment(content, post, token, context, test: true);
    });

    test('createPost should create a new post', () async {
      final result = HardData.allPosts[0];
      final users = [
        HardData.user1,
        HardData.user2,
      ];

      final post = postService.createPost(result, users, test: true);

      expect(post, isA<Post>());
    });

    test('typeEnum ad', () async {
      final typeId = 1;

      final result = postService.typeEnum(typeId);

      expect(result, PostTypeEnum.ad);
    });


    test('typeEnum advice', () async {
      final typeId = 2;

      final result = postService.typeEnum(typeId);

      expect(result, PostTypeEnum.advice);
    });

    test('typeEnum adviceRequest', () async {
      final typeId = 3;

      final result = postService.typeEnum(typeId);

      expect(result, PostTypeEnum.adviceRequest);
    });
  });
}
