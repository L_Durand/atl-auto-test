import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/add_post_page.dart';
import 'package:artosage/views/civil_post_list.dart';
import 'package:artosage/views/conversation_list.dart';
import 'package:artosage/views/location_page.dart';
import 'package:artosage/views/manage_account_page.dart';
import 'package:artosage/views/conversation_page.dart';
import 'package:artosage/views/offers_page.dart';
import 'package:artosage/views/pro_post_list.dart';
import 'package:artosage/views/profile_page.dart';
import 'package:artosage/views/register.dart';
import 'package:artosage/views/search_page.dart';
import 'package:artosage/views/login.dart';
import 'package:artosage/views/settings_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  // final _users = HardData.users;
  UserService userService = UserService();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'A\'Rosa-Je',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Accueil'),
      routes: {
        Login.routeName: (context) => Login(),
        Register.routeName: (context) => const Register(),
        CivilPostList.routeName: (context) => CivilPostList(),
        ProPostList.routeName: (context) => ProPostList(),
        AddPostPage.routeName: (context) => AddPostPage(),
        SearchPage.routeName: (context) =>  SearchPage(true, null),
        ProfilePage.routeName: (context) => ProfilePage(null),
        OffersPage.routeName: (context) =>  OffersPage(),
        SettingsPage.routeName: (context) =>  SettingsPage(),
        ManageAccountPage.routeName : (context) => ManageAccountPage(),
        // CivilPostPage.routeName: (context) =>  CivilPostPage(),
        LocationPage.routeName: (context) =>  LocationPage(),
        ConversationList.routeName: (context) =>  ConversationList(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({super.key, required this.title});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final StorageService storage = StorageService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: isUserTokenCheck(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          // Si le Future est en attente, afficher une indication de chargement
          return Scaffold(
            body: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: const Color(0xFF244653),
                image: DecorationImage(
                  image: const AssetImage('assets/images/background.png'),
                  repeat: ImageRepeat.repeat,
                  scale: 3,
                  colorFilter: ColorFilter.mode(
                    GlobalStyle.secondaryColor.withOpacity(0.8),
                    BlendMode.darken,
                  ),
                ),
              ),
              child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    "A'Rosa-Je",
                    style: GlobalStyle.pacificoRegularWhiteStyle,
                  )),
            ),
          );
        } else {
          // Si le Future a réussi, récupérer la valeur booléenne et l'afficher
          bool boolValue = snapshot.data!;
          if (boolValue) {
            return CivilPostList();
          } else {
            return Login();
          }
        }
      },
    );
  }

  Future<bool> isUserTokenCheck() async {
    bool isUserToken = await storage.containsKeyInSecureData('auth');
    if (isUserToken == true) {
      if (await storage.readSecureData('auth') == 'error') {
        isUserToken = false;
      }
    }
    return isUserToken;
  }
}
