import 'package:artosage/models/message.dart';
import 'package:artosage/models/user.dart';

class Conversation {
  User creator;
  User otherUser;
  List<Message> messages;

  Conversation(
      this.creator,
      this.otherUser,
      this.messages);

  Conversation.fromJson(Map<String, dynamic> json):
        creator = json['creator'],
        otherUser = json['otherUser'],
        messages = json['messages'];

}