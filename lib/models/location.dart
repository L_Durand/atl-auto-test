class Location{
  // int id;
  // String city;
  // String county;
  // String zipCode;
  double latitude;
  double longitude;

  Location(
    // this.id,
    // this.city,
    // this.county,
    // this.zipCode,
    this.latitude,
    this.longitude
  );
  Location.fromJson(Map<String, dynamic> json)
   :
        // id = json['id'],
    // city = json['city'],
    // county = json['county'],
    // zipCode = json['zip_code'],
    latitude = json['latitude'],
    longitude = json['longitude']
  ;
}