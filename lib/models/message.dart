import 'package:artosage/models/user.dart';

class Message{
  User author;
  String content;
  DateTime createdAt;

  Message(
    this.author,
    this.content,
    this.createdAt
  );

  Message.fromJson(Map<String, dynamic> json):
        author = json['author'],
        content = json['content'],
        createdAt = json['createdAt'];

}