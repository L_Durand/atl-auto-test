class File {
  final int id;
  final String path;

  const File({
    required this.id,
    required this.path,
  });

  factory File.fromJson(Map<String, dynamic> json) {
    return File(
      id: json['id'],
      path: json['path'],
    );
  }
}
