class Address{
  int id;
  int number;
  String street;
  String ?complement;
  String city;
  String zipCode;

  Address(
    this.id,
    this.number,
    this.street,
    this.complement,
    this.city,
    this.zipCode
  );
  Address.fromJson(Map<String, dynamic> json)
   : id = json['id'],
    number = json['number'],
    street = json['street'],
    complement = json['complement'],
    city = json['city'],
    zipCode = json['zip_code']
    ;
}