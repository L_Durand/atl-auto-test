class Right{
  String name;
  String code;

  Right(
    this.name, 
    this.code
  );
  Right.fromJson(Map<String, dynamic> json) 
  : name = json['name'],
  code = json['code'];
}