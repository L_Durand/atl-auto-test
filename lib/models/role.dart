import 'package:artosage/models/right.dart';

class Role{
  // int id;
  RoleType name;
  String code;
  List<Right> rights;

  Role(
    // this.id,
    this.name,
    this.code,
    this.rights
  );
  Role.fromJson(Map<String, dynamic> json)
  : name = json['name'],
  code = json['code'],
  rights = json['rights'];
}

enum RoleType{
  superUser,
  admin,
  user
}