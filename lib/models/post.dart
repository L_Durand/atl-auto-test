import 'package:artosage/models/file.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/models/comment.dart';

class Post {
  int id;
  String title;
  String description;
  List<String>? picPaths;
  DateTime createdAt;
  DateTime? updatedAt;
  List<Comment> comments;
  PostTypeEnum type;
  User author;
  List<User> offers;
  User? choice;

  Post(
    this.id,
    this.title,
    this.description,
    this.picPaths,
    this.createdAt,
    this.comments,
    this.type,
    this.author,
    this.offers,
    this.choice,
  );

  Post.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        description = json['description'],
        picPaths = json['files'],
        createdAt = json['created_at'],
        comments = json['comments'],
        type = json['type'],
        author = json['user_id'],
        offers = json['offers'],
        choice = json['choice'];
}

enum PostTypeEnum {
  ad('Annonce'),
  advice('Conseil'),
  adviceRequest('Demande de conseil');

  final String value;

  const PostTypeEnum(this.value);
}
