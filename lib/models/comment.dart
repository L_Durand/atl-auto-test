import 'package:artosage/models/user.dart';

class Comment{
  int id;
  String content;
  DateTime createdAt;
  DateTime ?updatedAt;
  User user;

  Comment(
    this.id,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.user
  );
  Comment.fromJson(Map<String, dynamic> json)
   : id = json['id'],
    content = json['text'],
    createdAt = json['created_at'],
    updatedAt = json['updated_at'],
    user = json['user_id']
  ;
}