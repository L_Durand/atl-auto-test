import 'package:artosage/models/address.dart';
import 'package:artosage/models/location.dart';

class User{
  int id;
  String firstName;
  String name;
  String email;
  DateTime? emailVerifiedAt;
  String password;
  String rememberToken;
  DateTime createdAt;
  DateTime? updatedAt;
  Location? location;
  // Role role;
  UserTypeEnum type;
  String profilePicFile; // ajouter feature: si l'user ne renseigne pas de pic, mettre un avatar par défaut tah facebook
  String bannerPicFile; // ajouter feature: si l'user ne renseigne pas de pic, mettre un avatar par défaut tah facebook
  Address address;
  bool isAddressShareAuthorized;

  User(
    this.id,
    this.firstName,
    this.name, 
    this.email, 
    this.emailVerifiedAt,
    this.password,
    this.rememberToken,
    this.createdAt,
    this.updatedAt,
    // this.role,
    this.type,
    this.profilePicFile,
    this.bannerPicFile,
    this.address,
    this.isAddressShareAuthorized,
    this.location,
  );
  User.fromJson(Map<String, dynamic> json)
   : id = json['id'],
        firstName = json['firstName'],
    name = json['name'],
    email = json['email'],
    emailVerifiedAt = json['email_verified_at'] ? json['email_verified_at'] : null,
    password = json['password'],
    rememberToken = json['remember_token'],
    createdAt = json['created_at'],
    updatedAt = json['updated_at'] ? json['updated_at'] : null,
    location = json['location'] ? json['location'] : null,
    // role = json['role_id'],
    profilePicFile = json['file_id'] ? json['file_id'] : 'assets/images/default.png',
    bannerPicFile = json['banner_id'] ? json['banner_id'] : 'assets/images/default.png',
    type = json['user_type'],
    address = json['address'] ? json['address'] : null,
    isAddressShareAuthorized = json['is_address_share_authorized'] ? json['is_address_share_authorized'] : null
  ;
}

enum UserTypeEnum {
  civil('Particulier'),
  pro('Botaniste');

  final String value;
  const UserTypeEnum(this.value);
}