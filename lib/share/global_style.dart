import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GlobalStyle {
  //Colors
  static const primaryColor = Color(0xff2A9D8F);
  static const secondaryColor = Color(0xff264653);
  static const backgroundColor = Color(0x0F2A9D8F);
  static const inputGrey = Color(0xffcfcfcf);
  static const thirdColor = Color(0xffe9c46a);
  static const transparentWhite = Color.fromARGB(200, 255, 255, 255);
  static final transparentPrimaryColor = primaryColor.withOpacity(0.1);

  //Fonts
  //Pacifico
  static final pacificoStyle = GoogleFonts.getFont('Pacifico');
  static final pacificoWhiteStyle = pacificoStyle.copyWith(color: Colors.white);
  static final pacificoRegularWhiteStyle = pacificoWhiteStyle.copyWith(fontSize: 32);
  //Inter
  static final interStyle = GoogleFonts.getFont('Inter');
  static final interPrimaryStyle = interStyle.copyWith(color: Colors.white, fontSize: 16);
  static final interBold = interPrimaryStyle.copyWith(fontWeight: FontWeight.w700);
  static final interSemiBold = interPrimaryStyle.copyWith(fontWeight: FontWeight.w600);
  static final interRegular = interPrimaryStyle.copyWith(fontWeight: FontWeight.w400);
  static final interCGU = interBold.copyWith(color: primaryColor);
}


