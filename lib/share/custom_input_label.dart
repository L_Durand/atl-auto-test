import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'global_style.dart';

class CustomInputLabel extends StatelessWidget {
  CustomInputLabel({@required this.text});
  var text;

  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
      child: Text(text,
          style: GlobalStyle.interRegular.copyWith(color: Colors.white70)),
    );
  }
}
