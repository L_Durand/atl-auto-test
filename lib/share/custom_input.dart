import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {
  CustomInput({required this.controller, required this.name});
  var controller;
  var name;
  Widget build(BuildContext context){
    return Container(
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        controller: controller,
        validator: (value) {
          if (name == 'facultatif'){
            return null;
          }
          if(value == null || value.isEmpty){
            return 'Veuillez renseigner $name.';
          } return null;
        },
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(10,0,10,0),
          filled: true,
          fillColor: Color(0xFFDDF4F2),
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}