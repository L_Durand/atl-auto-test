import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'global_style.dart';

class CustomPopup{

  static error(context, text) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: const Color(0xFFD40804).withOpacity(0.8),
            borderRadius: const BorderRadius.all(Radius.circular(8)),
            image: DecorationImage(
              image: const AssetImage('assets/images/errorBackground.png'),
              repeat: ImageRepeat.repeat,
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.2),
                BlendMode.srcOver,),
            ),
          ),
          child: Text(text, style: GlobalStyle.interRegular),
        ),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
    );
  }

  static validate(context, text) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Container(
          padding: const EdgeInsets.all(8),
          decoration: const BoxDecoration(
            color: GlobalStyle.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(8)),
            image: DecorationImage(
              image: AssetImage('assets/images/errorBackground.png'),
              repeat: ImageRepeat.repeat,
              fit: BoxFit.cover,
              opacity: (0.3),
            ),
          ),
          child: Text(text, style: GlobalStyle.interRegular),
        ),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
    );
  }
}
