import 'package:artosage/share/global_style.dart';
import 'package:artosage/views/share/bottom_navigation_bar_widget.dart';
import 'package:artosage/views/share/header_widget.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomScaffold extends StatefulWidget {
  final Widget widget;
  final bool isHeader;
  bool isFooter;
  final int footerIndex;
  final bool isGreenBackground;
  
  CustomScaffold(this.widget, this.isHeader, this.isFooter, this.footerIndex, this.isGreenBackground, {super.key});

  @override
  State<CustomScaffold> createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  @override
  Widget build(BuildContext context){
    if ((MediaQuery.of(context).size.width >= 1300) && (widget.isFooter)){
      setState(() {
        widget.isFooter = false;
      });
    }
    else if ((MediaQuery.of(context).size.width < 1300) && (widget.isFooter)){
      setState(() {
        widget.isFooter = true;
      });
    }

    PreferredSizeWidget header = PreferredSize(
      preferredSize: const Size.fromHeight(kToolbarHeight), 
      child: HeaderWidget(),
    );

    return Scaffold(
      appBar: widget.isHeader ? header : null,
      body: responsiveScaffold(context, widget.widget, false),
      bottomNavigationBar: widget.isFooter ? responsiveScaffold(context, BottomNavigationBarWidget(widget.footerIndex), true) : null,
      extendBody: true,
      backgroundColor: widget.isGreenBackground ? GlobalStyle.transparentPrimaryColor : Colors.white,
    );
  }

  Widget responsiveScaffold(BuildContext context, Widget childWidget, bool footer){
    late double width;
    double smallWidth = 576;
    double midWidth = 768;
    double largeWidth = 992;
    double xLargeWidth = 1200;

    if (MediaQuery.of(context).size.width <= 576){
      width = MediaQuery.of(context).size.width;
    }
    else {
      width = 576;
      if (MediaQuery.of(context).size.width > 992 && ModalRoute.of(context)!.settings.name == "/map_page") {
        width = 992;
      }
      else if(ModalRoute.of(context)!.settings.name == "/login" || ModalRoute.of(context)!.settings.name == "/register"){
        width = MediaQuery.of(context).size.width;
      }
    }

    if (footer){
      if ((MediaQuery.of(context).size.width > 721) && (MediaQuery.of(context).size.width <= 866)){
        width = width + width/4;
      }
      else if (MediaQuery.of(context).size.width > 866){
        width = width + width/2;
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: width,
          child: childWidget,
        ),
      ],
    );
  }
}