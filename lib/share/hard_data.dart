import 'package:artosage/models/address.dart';
import 'package:artosage/models/comment.dart';
import 'package:artosage/models/location.dart';
import 'package:artosage/models/message.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';

import '../models/conversation.dart';

class HardData {
  static List<Address> addresses = [
    Address(1, 12, "Plantade Street", null, "Montpellier", "34000"),
    Address(2, 58, "Rue nulle", null, "Clarensac", "30870")
  ];

  static List<String> pics = [
    'assets/images/UMLA.jpg',
    'assets/images/sneijder.jpg',
    'assets/images/alphawann.jpg'
  ];

  static User user1 = User(1, 'Benjamin', 'Hoarau', 'bh@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.civil, 'assets/images/ben_pdp.jpg', 'assets/images/random_banner.jpg', addresses[0], false, null);
  static User user2 = User(2, 'Thomas', 'Roig', 'tr@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.civil, 'assets/images/Wiltord.jpg', 'assets/images/sw_banner.jpg', addresses[1], true, Location(43.612469, 3.876303));
  static User user3 = User(3, 'Tom', 'Nougaillac', 'tn@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.civil, 'assets/images/default.png', 'assets/images/tn_banner.jpg', addresses[0], true, null);
  // static User user4 = User(4, 'Didier Roustan', 'dr@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.pro, 'assets/images/didier-roustan.jpeg', 'assets/images/didier-roustan.jpeg', addresses[0], false, null);
  static User user5 = User(5, 'Loïc', 'Durand', 'ld@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.pro, 'assets/images/loic_pdp.jpg', 'assets/images/random_banner.jpg', addresses[0], true, Location(43.612562, 3.878717));
  // static User user6 = User(6, 'Wendie Renard', 'wr@gmail.com', null, 'password', 'rememberToken', DateTime.now(), null, UserTypeEnum.civil, 'assets/images/wendie_renard.jpg', 'assets/images/wendie_renard.jpg', addresses[0], true, Location(3, 43.610472, 3.877798));

  static final List<User> users = [user1, user2, user3, user5];

  static final actualUser = user3;

  static List<Comment> comments = [
    Comment(1, 'Oui', DateTime.now(), null, user2),
    Comment(2, 'Che pense que on a fait um bom matche ce soir', DateTime.now(),null, user1),
    Comment(3, 'En effet Jean-Pierre, Paul nous a manqué ce soire', DateTime.now(), null, user1),
  ];

  static final List<Post> allPosts = [
    Post(1, 'Besoin de conseils pour mes rosiers', "Phasellus vulputate non odio nec fringilla. Quisque viverra velit eget vulputate facilisis. Nulla posuere ac velit ut volutpat. Integer rutrum tristique purus sed semper. Pellentesque sed diam vitae lacus mattis molestie. Nulla sagittis lectus vel tortor blandit, eget interdum ex feugiat. In ipsum tortor, feugiat et felis sed, condimentum placerat ipsum. Etiam pharetra pharetra eros id placerat. Curabitur non venenatis tellus, at vehicula tortor. ", ['assets/images/rosiers.jpg', 'assets/images/rosiers2.jpg'], DateTime.now(), [Comment(5, 'D\'où viennent vos rosiers ? J\'ai une grande connaissance des rosiers européens.', DateTime.now(), null, user5)], PostTypeEnum.adviceRequest, user1, [], null),
    Post(2, 'Besoin de faire garder mes plantes - longue durée', "Loïc Sed sed bibendum orci. Proin mollis tincidunt aliquet. Sed lacinia hendrerit eleifend. ", ['assets/images/rosiers.jpg', 'assets/images/rosiers2.jpg'], DateTime.now(), [], PostTypeEnum.ad, user1, [], null),
    Post(3, 'Annonce pour faire garder mon cactus pour 1 mois', "Donec justo elit, consequat ultricies mi et, malesuada convallis metus. Donec sed mollis elit. Nunc id gravida felis, sit amet vulputate sapien. In sagittis sem eget augue eleifend imperdiet. Morbi pulvinar tincidunt felis, vel molestie est ultrices non. Praesent consectetur urna nunc, ut aliquet diam euismod vel. Proin luctus feugiat mi non accumsan. Duis sit amet lacus non libero dapibus tempus. Pellentesque vitae sem finibus, aliquam neque venenatis, viverra magna. Aenean faucibus congue suscipit. Aenean ac magna iaculis ipsum euismod viverra. ", ['assets/images/cactus.jpg'], DateTime.now(), [Comment(4, 'Joli cactus !', DateTime.now(), null, user5), Comment(6, 'Il me faudrait plus de précisions sur votre cactus s\'il vous plaît, je pourrait m\'en occuper.', DateTime.now(), null, user1),], PostTypeEnum.ad, user3, [user1], null),
    // Post(4, 'Yes', "Yes.", ['assets/images/mcrae.jpg'], DateTime.now(), [], PostTypeEnum.ad, user3, [user1, user5], null),
    Post(5, "Entretenir un monstera", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac dui nec erat aliquam ornare. Sed et risus sed diam tempus volutpat. Donec aliquam orci est, sit amet imperdiet neque dapibus quis. Sed et turpis ut massa facilisis condimentum. Integer consequat, nulla eu mattis sagittis, nunc nulla accumsan nisi, eget ultricies elit enim nec tortor. Nunc semper erat neque, ac eleifend lorem porta sed. Aliquam a luctus sem.", ['assets/images/monstera.jpg', 'assets/images/monstera2.jpg'], DateTime.now(), comments, PostTypeEnum.advice, user3, [], null),
    // Post(6, "Tuto passe", "Appuyez vos passes bordel", ['assets/images/Kroos_passe.jpg', 'assets/images/Iniesta_passe.jpg'], DateTime.now(), [], PostTypeEnum.advice, user5, [], null),
  ];

  static final List<Post> civilPosts = allPosts.where((i) => i.type==PostTypeEnum.ad || i.type==PostTypeEnum.adviceRequest).toList();
  static final List<Post> proPosts = allPosts.where((i) => i.type==PostTypeEnum.advice).toList();

  static List<Conversation> allConversations = [
    Conversation(user3, user1, [Message(user3, "yo", DateTime(2023, 07, 10, 16, 03, 10, 216)), Message(user1, "yoyoyo", DateTime(2023, 07, 10, 16, 04, 10, 216)), Message(user3, "ça va ou quoi", DateTime(2023, 07, 10, 16, 53, 10, 216)), Message(user1, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user3, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user1, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user1, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user1, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user1, "ça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216)), Message(user1, "ça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoiça va ou quoi", DateTime(2023, 07, 10, 16, 54, 10, 216))]),
    Conversation(user3, user2, [Message(user2, "heyo", DateTime(2023, 07, 10, 16, 13, 10, 216)), Message(user3, "hey", DateTime(2023, 07, 10, 16, 14, 10, 216))])
  ];
}