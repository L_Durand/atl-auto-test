import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../service/user_service.dart';

class CustomPasswordInput extends StatefulWidget {
  CustomPasswordInput({required this.controller});
  var controller;
  @override
  State<CustomPasswordInput> createState() => _CustomPasswordInput(controller: controller);
}

class _CustomPasswordInput extends State<CustomPasswordInput> {
  _CustomPasswordInput({required this.controller});
  var controller;

  bool passwordVisible = false;

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }


  Widget build(BuildContext context){
    return Container(
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        obscureText: passwordVisible,
        controller: controller,
        validator: (value) {
          if(value == null || value.isEmpty){
            return 'Veuillez renseigner un mot de passe.';
          } return null;
        },
        decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(10,0,10,0),
          suffixIcon: IconButton(
            icon: Icon(passwordVisible
                ? Icons.visibility
                : Icons.visibility_off),
            onPressed: () {
              setState(
                    () {
                  passwordVisible = !passwordVisible;
                },
              );
            },
          ),
          filled: true,
          fillColor: Color(0xFFDDF4F2),
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}