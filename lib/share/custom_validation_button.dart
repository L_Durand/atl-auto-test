import 'package:flutter/material.dart';

import 'global_style.dart';

class CustomValidationButton extends StatelessWidget {
  const CustomValidationButton({super.key, required this.text, required this.onPressed, this.horizontalMargin, this.verticalMargin});
  final GestureTapCallback onPressed;
  final double? horizontalMargin;
  final double? verticalMargin;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(horizontalMargin ?? 10, verticalMargin ?? 20, horizontalMargin ?? 10, verticalMargin ?? 10),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xFFF25945),
          ),
          onPressed: onPressed,
          child: Text(
            text.toUpperCase(),
            style: GlobalStyle.interBold,
          ),
        ));
  }
}