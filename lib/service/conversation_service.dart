import 'dart:convert';

import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_popup.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:artosage/models/conversation.dart';

import '../models/message.dart';
import '../views/conversation_list.dart';
import '../views/conversation_page.dart';

class ConversationService{
  List<Conversation> conversations = [];
  // List<Message> messages = [];
  static final data = []; // prochainement une HTPP REQUEST
  static final server = "http://localhost:8081";

  static List<Conversation> buildList() {
    List<Conversation> list = data.map((item) =>
        Conversation.fromJson(item)).toList();
    return list;
  }

  Future<List<Conversation>> getAllConversations(token, users) async {
    http.Response response =
    await http.get(Uri.parse('$server/api/chats/conversations'), headers: {
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
      for (var result in results) {
        await getConversation(token, result['receiver_id'], users);
      }
    } else {
      throw Exception('Failed to load Conversation');
    }
    return conversations;
  }

  Future<Conversation> getConversation(token, receiver, users) async {
    var conversation;
    var creator;
    var creatorUser;
    var receiverUser;
    List<Message> messages = [];

    http.Response response =
    await http.get(Uri.parse('$server/api/chats/$receiver'), headers: {
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
      if (results[0]['user_id'] == receiver) {
        creator = results[0]['receiver_id'];
      } else {
        creator = results[0]['user_id'];
      }
      for (var user in users) {
        if (user.id == creator) {
          creatorUser = user;
        }
        if (user.id == receiver){
          receiverUser = user;
        }
      }
      for (var result in results) {
        var formatedResult = createMessage(result, users);
        messages.insert(0, formatedResult);
      }
    } else {
      throw Exception('Failed to load Conversation');
    }
    conversation = Conversation(
        creatorUser,
        receiverUser,
        messages
    );
    conversations.insert(0, conversation);
    return conversation;
  }

  Future<void> addMessage(content, receiver, token, conversation, context, activeUser) async {
    http.Response response = await http.post(
      Uri.parse('$server/api/chats'),
      headers: {
        'Authorization': token,
      },
      body: {'receiver_id': receiver, 'message': content},
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to load user');
    }
  }

  Message createMessage(message, users) {
    var author;
    for (var user in users) {
      if (user.id == message['user_id']) {
        author = user;
      }
    }
    var formatedMessage = Message(
      author,
      message['message'],
      DateTime.parse(message['created_at']),
    );
    return formatedMessage;
  }
}