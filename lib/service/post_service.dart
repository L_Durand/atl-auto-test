import 'dart:convert';

import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_popup.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:http/http.dart' as http;

import '../models/address.dart';
import '../models/user.dart';
import '../models/comment.dart';

class PostService {
  var posts;
  static final data = []; // prochainement une HTPP REQUEST
  static final server = "http://localhost:8081";

  PostService() {
    posts = PostService.buildList();
  }

  List<Post> getPosts({bool? test}) {
    if (test == true) {
      posts = [
        HardData.allPosts[0],
        HardData.allPosts[1],
      ];
      return posts;
    }
    return posts;
  }

  static List<Post> buildList() {
    List<Post> list = data.map((item) => Post.fromJson(item)).toList();
    return list;
  }

  Future<List<Post>> getAllPosts(token, type, users, {bool? test}) async {
    if (test == true) {
      posts = HardData.allPosts;
      return posts;
    }

    posts = [];

    http.Response response =
        await http.get(Uri.parse('$server/api/posts'), headers: {
      'Authorization': token,
    });

    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
      for (var result in results) {
        var formatedResult = createPost(result, users);
        if (type.contains(formatedResult.type)) {
          posts.insert(0, formatedResult);
        }
      }
    } else {
      throw Exception('Failed to load user');
    }
    return posts;
  }

  Future<void> saveNewPost(
      context, token, idUser, postType, description, selectedFiles,
      {bool? test}) async {
    if (test == true) {
      return;
    }
    Map<String, String> dataBody = {
      'description': description,
      'user_id': idUser,
      'type_post_id': postType
    };
    final request = http.MultipartRequest(
      'POST',
      Uri.parse('$server/api/posts'),
    );

    request.headers.addAll({'Authorization': token});

    request.fields.addAll(dataBody);

    for (final file in selectedFiles) {
      final bytes = await file.bytes!;
      request.files.add(http.MultipartFile.fromBytes(
        'files[]',
        bytes,
        filename: file.name,
      ));
    }
    CustomPopup.validate(context, 'Publication réussie');
    final response = await request.send();
  }

  Future<void> addComment(content, post, token, context, {bool? test}) async {
    if (test == true) {
      return;
    }
    http.Response response = await http.post(
      Uri.parse('$server/api/comments'),
      headers: {
        'Authorization': token,
      },
      body: {'text': content, 'post_id': post},
    );

    if (response.statusCode == 200) {
      CustomPopup.validate(context, 'Commentaire ajouté');
    } else {
      throw Exception('Failed to load user');
    }
  }

  Post createPost(post, users, {bool? test}) {
    var postUser;

    if (test == true) {
      return Post(
        1,
        '',
        'description',
        [],
        DateTime.now(),
        [],
        typeEnum(1),
        HardData.user1,
        [],
        null,
      );
    }

    for (var user in users) {
      if (user.id == post['user_id']) {
        postUser = user;
      }
    }
    List<String> picList = [];
    for (var pic in post['files']) {
      var path = '$server/${pic['path']}';
      picList.add(path);
    }
    List<Comment> commentList = [];
    for (var comment in post['comments']) {
      var commentUser;
      for (var user in users) {
        if (user.id == comment['user_id']) {
          commentUser = user;
        }
      }
      var commentFormated = Comment(
          comment['id'],
          comment['text'],
          DateTime.parse(comment['created_at']),
          DateTime.parse(comment['updated_at']),
          commentUser);
      commentList.add(commentFormated);
    }
    var formatedPost = Post(
      post['id'],
      '',
      post['description'],
      picList,
      DateTime.parse(post['created_at']),
      commentList,
      typeEnum(post['type_post_id']),
      postUser,
      [],
      null,
    );
    return formatedPost;
  }

  PostTypeEnum typeEnum(typeId) {
    PostTypeEnum type;
    switch (typeId) {
      case 1:
        type = PostTypeEnum.ad;
        break;
      case 2:
        type = PostTypeEnum.advice;
        break;
      default:
        type = PostTypeEnum.adviceRequest;
    }
    return type;
  }
}
