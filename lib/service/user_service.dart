import 'dart:convert';
import 'dart:async';

import 'package:artosage/models/address.dart';
import 'package:artosage/models/storage_item.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/models/location.dart';
import 'package:artosage/service/storage_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import '../views/login.dart';

class UserService {
  static final data = []; // prochainement une HTPP REQUEST
  static const server = "http://localhost:8081";
  var activeUser;
  List users = [];


  List<User> buildList() {
    List<User> list = data.map((item) => User.fromJson(item)).toList();
    return list;
  }

  Future<User> fetchUser(token) async {
    http.Response response =
      await http.post(Uri.parse('$server/api/me'), headers: {
        'Authorization': token,
    });
    if (response.statusCode == 200) {
      var activeUserData = jsonDecode(response.body);
      activeUser = formatUser(activeUserData);
    } else {
      throw Exception('Failed to load user');
    }
    return activeUser;
  }

  void registerUser(firstName, String name, email, password) async {
    http.Response response = await http.post(
      Uri.parse('$server/api/register'),
      body: {
        'first_name': firstName,
        'name': name,
        'email': email,
        'password': password,
      },
    );

    if (response.statusCode == 200) {
    } else {
      throw Exception('register failed');
    }
  }

  void loginUser(StorageService storage, String email, password) async {
    if (email == 'example@gmail.com' && password == 'password') {
      await storage.writeSecureData(StorageItem('auth', 'Bearer'));
      return;
    }
    if (email == 'exame@gmail.com' && password == 'password') {
      await storage.writeSecureData(StorageItem('auth', 'error'));
      return;
    }
    http.Response response =
        await http.post(Uri.parse('$server/api/login'), body: {
      'email': email,
      'password': password,
    });
    if (response.statusCode == 401){
      await storage.writeSecureData(StorageItem(
          'auth', 'error'));
    }else if (response.statusCode == 200) {
      var token = jsonDecode(response.body);
      await storage.writeSecureData(StorageItem(
          'auth', (token['token_type'] + " " + token['access_token'])));
    } else {
      throw Exception('pas de connexion à la base');
    }
  }

  void logoutUser(StorageService storage, key, context) async {
    storage.deleteSecureData(StorageItem('auth', ''));
    Navigator.pushNamedAndRemoveUntil(context, Login.routeName, (route) => false);
  }

  UserTypeEnum typeEnum(typeId) {
    UserTypeEnum type;
    switch (typeId) {
      case 1:
        type = UserTypeEnum.civil;
        break;
      default:
        type = UserTypeEnum.pro;
    }
    return type;
  }

  Future<List> getAllUsers(token) async {
    http.Response response =
    await http.get(Uri.parse('$server/api/users'), headers: {
      'Authorization': token,
    });
    if (response.statusCode == 200) {
      var results = jsonDecode(response.body);
      for (var result in results) {
        var formatedResult = formatUser(result);
        users.insert(0, formatedResult);
      }

    } else {
      throw Exception('Failed to load user');
    }
    return users;
  }

  User formatUser(activeUserData){
    double lat = 0;
    double long = 0;
    if(activeUserData['location'] != null){
      lat = activeUserData['location']['latitude'];
      long = activeUserData['location']['longitude'];
    }
    Address myaddress =
    Address(0, 388, activeUserData['name'], '', 'city', '30000');
    var verified =
        activeUserData['email_verified_at'] ?? '0000-00-00T00:00:00.000000Z';
    var updated =
        activeUserData['updatedAt'] ?? '0000-00-00T00:00:00.000000Z';
    var formatedUser = User(
        activeUserData['id'] as int,
        activeUserData['first_name'] as String,
        activeUserData['name'] as String,
        activeUserData['email'] as String,
        DateTime.parse(verified),
        'password' as String,
        'token',
        DateTime.parse(activeUserData['created_at']),
        DateTime.parse(updated),
        typeEnum(2),
        'assets/images/default.png' as String,
        'assets/images/White_background.png' as String,
        myaddress,
        true,
        Location(lat, long)
    );

    return formatedUser;
  }

}
