import 'package:artosage/models/post.dart';
import 'package:artosage/service/conversation_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/civil_post_list.dart';
import 'package:artosage/views/share/comments_widget.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';

import '../models/user.dart';
import '../service/user_service.dart';

class CivilPostPage extends StatefulWidget {
  static String routeName = '/civil_post';
  final Post post;
  User actualUser;
  UserService userService = UserService();
  ConversationService conversationService = ConversationService();
  String? token;
  CivilPostPage(this.post, this.actualUser, this.token, {super.key});

  @override
  State<CivilPostPage> createState() => _CivilPostPageState();
}

class _CivilPostPageState extends State<CivilPostPage> {
  @override
  Widget build(BuildContext context){
    return CustomScaffold(
      ListView(
        children:[
          PostWidget(widget.actualUser, widget.post, true, widget.token, widget.conversationService.conversations),
          SimpleCommentsWidget(widget.actualUser, widget.post, false, true, widget.token),
        ],
      ), 
      true, 
      false, 
      0,
      false
    );
  }
}