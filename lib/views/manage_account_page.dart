import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:flutter/material.dart';

class ManageAccountPage extends StatefulWidget {
  static String routeName = '/manage_account';
  String? token;
  UserService userService = UserService();

  ManageAccountPage({super.key});

  @override
  State<ManageAccountPage> createState() => _ManageAccountPageState();
}

class _ManageAccountPageState extends State<ManageAccountPage> {
  StorageService storage = StorageService();
  bool isModaleVisible = false;

  @override
  Widget build(BuildContext context){
    return FutureBuilder<List>(
        future: getContent(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return CustomScaffold(
                const Center(
                    child: CircularProgressIndicator(
                      color: GlobalStyle.primaryColor,
                    )),
                true,
                true,
                0,
                false);
          } else {
            // Si le Future a réussi, récupérer la valeur booléenne et l'afficher
            return CustomScaffold(_buildManageAccountPage(), true, false, 4, true);
          }
        }
    );
    return CustomScaffold(_buildManageAccountPage(), true, false, 4, true);
  }

  Widget _buildManageAccountPage(){
    return Container(
      margin: const EdgeInsets.fromLTRB(15, 45, 15, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildMarginContainer(
            _buildTextInkWell(
              "Déconnexion",
              () => widget.userService.logoutUser(storage, 'auth', context),
            ),
            15
          ),
          _buildMarginContainer(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildTextInkWell(
                  "Supprimer mon compte", 
                  () => setState(() {
                    isModaleVisible=true;
                  })
                ),
                _buildMarginContainer(
                  Text(
                    "Après cette action votre compte sera supprimé.\nVous pourrez néanmoins en créer un nouveau ultérieurement.",
                    style: GlobalStyle.interRegular
                      .copyWith(color: GlobalStyle.secondaryColor)
                      .copyWith(fontSize: 14),
                  ), 
                  5,
                ),
              ]
            ),
            20
          ),
          Visibility(
            visible: isModaleVisible,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: GlobalStyle.secondaryColor, 
                      width: 1
                    ), 
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  width: 230,
                  height: 90,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          "Êtes-vous sûr ?", 
                          style: GlobalStyle.interBold
                            .copyWith(fontSize: 16)
                            .copyWith(color: GlobalStyle.secondaryColor),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                            onPressed: () => print('oui'),
                            style: TextButton.styleFrom(
                              backgroundColor: GlobalStyle.secondaryColor,
                              foregroundColor: Colors.white,
                              textStyle: GlobalStyle.interBold,
                              padding: const EdgeInsets.all(15)
                            ),
                            child: const Text("Oui"),
                          ),
                          TextButton(
                            onPressed: () {
                              print('non');
                              setState(() {
                                isModaleVisible = false;
                              });
                            }, 
                            style: TextButton.styleFrom(
                              backgroundColor: GlobalStyle.primaryColor,
                              foregroundColor: Colors.white,
                              textStyle: GlobalStyle.interBold,
                              padding: const EdgeInsets.all(15)
                            ),
                            child: const Text("Non"),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            )
          ),
        ],
      ),
    );
  }

  Widget _buildMarginContainer(Widget childWidget, double verticalMargin){
    return Container(
      margin: EdgeInsets.symmetric(vertical: verticalMargin),
      child: childWidget,
    );
  }

  Widget _buildTextInkWell(String text, void Function() callback){
    return InkWell(
      onTap: callback,
      child: Text(
        text,
        style: GlobalStyle.interBold
          .copyWith(color: GlobalStyle.secondaryColor)
          .copyWith(fontSize: 18),
      )
    );
  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    return [];
  }
}