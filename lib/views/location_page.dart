import 'dart:ui';
import 'package:artosage/models/location.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_map/flutter_map.dart';
import "package:latlong2/latlong.dart";

class LocationPage extends StatefulWidget {
  static String routeName = "/map_page";
  UserService userService = UserService();
  String? token;
  List users = [];
  var actualUser;

  LocationPage({super.key});

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  StorageService storage = StorageService();
  Position? _currentPosition;
  final MapController _mapController = MapController();
  List<Marker> markers = [];
  bool isMapVisible = false;

  @override
  void initState() {
    super.initState();

    isMapVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
        future: getContent(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return const Center(
                child: CircularProgressIndicator(
              color: GlobalStyle.primaryColor,
            ));
          } else {
            return CustomScaffold(
                SafeArea(
                  child: ListView(
                    children: [
                      SizedBox(
                        width: 200,
                        height: 80,
                        child: CustomValidationButton(
                          text: "Trouver ma localisation",
                          onPressed: () {
                            _getCurrentPosition();
                            setState(() {
                              isMapVisible = true;
                            });
                          },
                          horizontalMargin: 50,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 15),
                        child: SizedBox(
                          height: 600,
                          width: 1000,
                          child: _buildMap(),
                        ),
                      ),
                    ],
                  ),
                ),
                true,
                true,
                2,
                false);
          }
        });
  }

  Widget _buildMap() {
    return Visibility(
      visible: isMapVisible,
      child: FlutterMap(
        mapController: _mapController,
        options: MapOptions(
          center: LatLng(43.642586, 3.838258),
          zoom: 12,
          maxZoom: 18,
        ),
        children: [
          TileLayer(
            urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            userAgentPackageName: 'com.example.app',
          ),
          MarkerLayer(

            markers: markers,
          ),
        ],
      ),
    );
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              "Le service de géolocalisation est désactivé. Veuillez l'activer pour profiter de cette fonctionnalité.")));
      return false;
    }
    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('La géolocalisation a été refusée pour le moment.')));
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('La géolocalisation a été refusée pour toujours.')));
      return false;
    }
    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    // widget.actualUser.isAddressShareAuthorized = hasPermission;
    if (!hasPermission) return;

    await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.lowest,
    ).then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      widget.actualUser.location =
          Location(_currentPosition!.latitude, _currentPosition!.longitude);
      widget.users.add(widget.actualUser);
      // HardData.actualUser.location?.latitude = _currentPosition!.latitude;
      // HardData.actualUser.location?.longitude = _currentPosition!.longitude;
      _addAllMarkers();
      _mapController.move(
        LatLng(widget.actualUser.location.latitude, widget.actualUser.location.longitude), 16
      );
    }).catchError((e) {
      print(e);
    });
  }

  void _addAllMarkers() {
    print('addallmarkers');
    print(widget.users);
    List usersWithLocation = widget.users
        .where((i) => i.isAddressShareAuthorized && i.location != null)
        .toList();

    for (var user in usersWithLocation) {
      markers.add(
        Marker(
          point: LatLng(user.location!.latitude, user.location!.longitude),
          builder: (context) => _buildMarker(user),
          width: 230,
          height: 230,
        ),
      );
    }
  }

  Widget _buildMarker(User user) {
    return SizedBox(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          Image(
            image: AssetImage('assets/images/green-circle.png'),
            // color: GlobalStyle.primaryColor,
            opacity: AlwaysStoppedAnimation(.2),
            width: 100,
            height: 100,
          ),
          // Container(
          //   padding: const EdgeInsets.all(4),
          //   decoration: BoxDecoration(
          //     borderRadius: const BorderRadius.all(Radius.circular(8)),
          //     border: Border.all(color: GlobalStyle.secondaryColor),
          //     color: GlobalStyle.primaryColor,
          //   ),
          //   // child: ProfileInkwell(
          //   //   Text(
          //   //     '${user.firstName} ${user.name}',
          //   //     style: const TextStyle(
          //   //       color: Colors.white,
          //   //       fontSize: 18,
          //   //       fontFamily: 'Inter',
          //   //     ),
          //   //   ),
          //   //   user,
          //   //   true,
          //   // ),
          // ),
        ],
      ),
    );
  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    widget.actualUser = await widget.userService.fetchUser(widget.token);
    widget.users = await widget.userService.getAllUsers(widget.token);
    return [widget.actualUser];
  }
}
