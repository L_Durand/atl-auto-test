import 'package:artosage/models/user.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_popup.dart';
import 'package:artosage/share/custom_input.dart';
import 'package:artosage/share/custom_input_label.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/views/civil_post_page.dart';
import 'package:custom_check_box/custom_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../share/custom_password_input.dart';
import 'login.dart';

class Register extends StatefulWidget {
  static String routeName = '/register';

  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _firstNameController = TextEditingController();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool isChecked = false;
  final UserService userService = UserService();
  var controller;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    isChecked = false;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
        Container(
          padding: MediaQuery
              .of(context)
              .size
              .width > 992 ? EdgeInsets.symmetric(
              vertical: 10, horizontal: MediaQuery
              .of(context)
              .size
              .width / 6) : const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: const Color(0xFF244653),
            image: DecorationImage(
              image: const AssetImage('assets/images/background.png'),
              repeat: ImageRepeat.repeat,
              scale: 3,
              colorFilter: ColorFilter.mode(
                GlobalStyle.secondaryColor.withOpacity(0.8),
                BlendMode.darken,
              ),            ),
          ),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      "A'Rosa-Je",
                      style: GlobalStyle.pacificoRegularWhiteStyle,
                    )),
                CustomInputLabel(text: 'Adresse e-mail'),
                CustomInput(
                    controller: _emailController, name: 'une adresse e-mail'),
                CustomInputLabel(text: 'Prénom'),
                CustomInput(
                    controller: _firstNameController, name: 'un prénom'),
                CustomInputLabel(text: 'Nom'),
                CustomInput(controller: _nameController, name: 'un nom'),
                CustomInputLabel(text: 'Mot de passe'),
                CustomPasswordInput(controller: _passwordController),
                Row(
                  children: [
                    CustomCheckBox(
                      uncheckedIconColor: Color(0xFFDDF4F2),
                      uncheckedFillColor: Color(0xFFDDF4F2),
                      checkedFillColor: Color(0xFFF25945),
                      borderWidth: 0,
                      value: isChecked,
                      shouldShowBorder: true,
                      checkBoxSize: 24,
                      onChanged: (val) {
                        setState(() {
                          isChecked = val;
                        });
                      },
                    ),
                    TextButton(
                      onPressed: () {
                        openCGU();
                      },
                      child: Text(
                        "J'accepte les conditions d'utilisation",
                        style: GlobalStyle.interRegular,
                      ),
                    ),
                  ],
                ),
                CustomValidationButton(
                  text: 'créer mon compte',
                  onPressed: () {
                    if (isChecked) {
                      if (!(_formKey.currentState!.validate())) {} else {
                        userService.registerUser(
                            _firstNameController.text.toString(),
                            _nameController.text.toString(),
                            _emailController.text.toString(),
                            _passwordController.text.toString());
                        Navigator.pushNamedAndRemoveUntil(
                            context, Login.routeName, (route) => false);
                      }
                    } else {
                      CustomPopup.error(context,
                          "veuillez accepter les conditions d'utilisation");
                    }
                  },
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, Login.routeName, (route) => false);
                    },
                    child: Text(
                      'Déjà un compte ?',
                      style: GlobalStyle.interSemiBold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        false,
        false,
        2,
        false
    );
  }

  Future openCGU() => showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
          'Conditions d\'Utilisation',
          style: GlobalStyle.interCGU,
        ),
        content: Text(
          'Le site propose un service de gardiennage de plante, '
              'ainsi que la mise en relation des utilisateurs afin '
              'de prodiguer differents conseils. \n'
              'Aucune donnée personnelle ne sera transmise '
              'au public hormis votre nom et prenom sans votre permission.\n'
              'Vous pouvez à tout moment si vous le souhaitez effacer votre '
              'compte.\n'
              'Le site peut appliquer une modération en cas d\'abus. Nous ne '
              'garantissons pas que l\'intention d\'utilisateur non '
              'professionnel ne soit pas malveillante. \n'
              'Aucun mot de passe ne sera demandé par l\'équipe de modération '
              'et nous déclinons toute responsabilité en cas utilisation de '
              'sources extérieure ou de lien menant hors du site.',
        )

      ),
  );
}
