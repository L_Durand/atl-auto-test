import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/conversation_service.dart';
import 'package:artosage/service/post_service.dart';
import 'package:artosage/service/storage_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/profile_widget.dart';
import 'package:flutter/material.dart';
import '../models/conversation.dart';
import '../service/user_service.dart';
import '../share/global_style.dart';

class ProfilePage extends StatefulWidget {
  static String routeName = '/profile';
  var profileUser;
  UserService userService = UserService();
  PostService postService = PostService();
  ConversationService conversationService = ConversationService();
  String? token;

  ProfilePage(this.profileUser, {super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late int profileIndex;
  StorageService storage = StorageService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
        future: getUser(),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return CustomScaffold(
                const Center(
                    child: CircularProgressIndicator(
                  color: GlobalStyle.primaryColor,
                )),
                true,
                true,
                0,
                false);
          } else {
            // Si le Future a réussi, récupérer la valeur booléenne et l'afficher
            if (widget.profileUser == null) {
              widget.profileUser = widget.userService.activeUser;
              profileIndex = 4;
            } else {
              profileIndex = 0;
            }
            return CustomScaffold(
                ProfileWidget(widget.profileUser, widget.userService.activeUser,
                    widget.postService.posts, widget.conversationService.conversations, widget.token),
                true,
                true,
                profileIndex,
                false);
          }
        });
  }

  Future<User> getUser() async {
    widget.token = await storage.readSecureData('auth');
    User actualUser = await widget.userService.fetchUser(widget.token);
    List users = await widget.userService.getAllUsers(widget.token);
    await widget.postService.getAllPosts(widget.token, [PostTypeEnum.adviceRequest, PostTypeEnum.ad, PostTypeEnum.advice], users);
    List<Conversation> conversations = await widget.conversationService.getAllConversations(
        widget.token, users);
    return actualUser;
  }
}
