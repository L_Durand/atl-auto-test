import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';

import '../models/conversation.dart';
import '../models/user.dart';
import '../service/conversation_service.dart';
import '../service/post_service.dart';
import '../service/storage_service.dart';
import '../service/user_service.dart';
import '../share/global_style.dart';

class ProPostList extends StatefulWidget {
  static String routeName = '/pro_posts';
  // final List<Post> _proPosts = HardData.proPosts;
  UserService userService = UserService();
  PostService postService = PostService();
  ConversationService conversationService = ConversationService();

  String? token;

  ProPostList({super.key});

  @override
  State<ProPostList> createState() => _ProPostListState();
}

class _ProPostListState extends State<ProPostList> {
  final StorageService storage = StorageService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
        future: getContent(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return CustomScaffold(
                const Center(
                    child: CircularProgressIndicator(
                      color: GlobalStyle.primaryColor,
                    )),
                true,
                true,
                0,
                false);
          } else {
            // Si le Future a réussi, récupérer la valeur booléenne et l'afficher
            return CustomScaffold(
                PostListWidget(
                    widget.userService.activeUser, widget.postService.posts, widget.conversationService.conversations, widget.token),
                true,
                true,
                0,
                false);
          }
        }
    );  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    User actualUser = await widget.userService.fetchUser(widget.token);
    List users = await widget.userService.getAllUsers(widget.token);
    List<Post> posts = await widget.postService.getAllPosts(widget.token, [PostTypeEnum.advice], users);
    List<Conversation> conversations = await widget.conversationService.getAllConversations(
        widget.token, users);
    return [actualUser, posts];
  }
}