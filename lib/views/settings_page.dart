import 'dart:io';
import 'package:artosage/models/user.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:custom_check_box/custom_check_box.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SettingsPage extends StatefulWidget {
  final User user = HardData.actualUser;
  static String routeName = '/settings';

  SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final _formKey = GlobalKey<FormState>();
  String newBannerPicPath = "";
  String newProfilePicPath = "";
  ImagePicker imagePicker = ImagePicker();
  late TextEditingController emailController = TextEditingController(text: widget.user.email);
  late TextEditingController surnameController = TextEditingController(text: widget.user.name);
  // late TextEditingController firstnameController = TextEditingController();
  late TextEditingController cityController = TextEditingController(text: widget.user.address.city);
  late bool checkboxVal = widget.user.isAddressShareAuthorized;
  bool isHover = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      _buildSettingsPage(),
      true,
      false,
      4,
      true
    );
  }

  Widget _buildSettingsPage(){
    return Container(
      margin: const EdgeInsets.all(15),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 5),
              child: GestureDetector(
                onTap: () async {
                  newBannerPicPath = await takeImage(ImageSource.gallery);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ConstrainedBox(
                      constraints: const BoxConstraints(maxHeight: 150),
                      child: Image.asset(
                        widget.user.bannerPicFile,
                        fit: BoxFit.fitWidth,
                        alignment: Alignment.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () async {
                      newProfilePicPath = await takeImage(ImageSource.gallery);
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: SizedBox(
                        width: 60,
                        height: 60,
                        child: CircleAvatar(
                          backgroundColor: Colors.red,
                          backgroundImage: AssetImage(
                            widget.user.profilePicFile
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: _buildTextInput(emailController, "E-mail", "Veuillez entrer une adresse E-mail.", false),
                  ),
                ],
              ),
            ),
            _buildTextInput(surnameController, "Nom", "Veuillez entrer un nom.", true),
            // _buildTextInput(firstnameController, "Prénom", widget.user.name, "Veuillez entrer un prénom."),
            _buildTextInput(cityController, "Ville", "Veuillez entrer une ville.", true),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 36,
                  height: 36,
                  child: CustomCheckBox(
                    uncheckedIconColor: Colors.white,
                    uncheckedFillColor: Colors.white,
                    checkedFillColor: const Color(0xFFF25945),
                    borderWidth: 0,
                    borderColor: GlobalStyle.transparentPrimaryColor,
                    value: checkboxVal,
                    shouldShowBorder: true,
                    checkBoxSize: 20,
                    onChanged: (newValue) {
                      setState((){
                        checkboxVal = newValue;
                      });
                    },
                  ),

                ),
                Text(
                  "Je rends ma localisation publique",
                  style: GlobalStyle.interBold
                    .copyWith(color: GlobalStyle.secondaryColor)
                    .copyWith(fontSize: 14),
                ),
              ],
            ),
            CustomValidationButton(
              text: 'Sauvegarder', 
              horizontalMargin: 0,
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState?.save();

                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Sauvegarde...')),
                  );

                  if (newProfilePicPath != ""){
                    widget.user.profilePicFile = newProfilePicPath;
                  }
                  if (newBannerPicPath != ""){
                    widget.user.bannerPicFile = newBannerPicPath;
                  }

                  widget.user.name = surnameController.text;
                  widget.user.address.city = cityController.text;
                  widget.user.isAddressShareAuthorized = checkboxVal;

                  if (checkboxVal == false){
                    widget.user.location = null;
                  }
                }
              },
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/manage_account');
              },
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  "Gestion du compte",
                  style: GlobalStyle.interBold
                    .copyWith(color: GlobalStyle.secondaryColor)
                    .copyWith(fontSize: 16),
                ),
              ),

            )
          ]
        ),
      ),
    );
  }

  Widget _buildTextInput(TextEditingController controller, String placeholder, String errorText, bool isEnabled){
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        enabled: isEnabled,
        // initialValue: initialValue,
        controller: controller,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return errorText;
          }
          return null;
        },
        decoration: InputDecoration(
          fillColor: isEnabled ? Colors.white : const Color.fromARGB(255, 228, 228, 228),
          filled: true,
          isDense: true,
          contentPadding: const EdgeInsets.all(10),
          hintText: placeholder,
          hintStyle: GlobalStyle.interRegular
            .copyWith(color: GlobalStyle.inputGrey)
            .copyWith(fontSize: 15),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }

  Future<String> takeImage(ImageSource source) async {
    final XFile? imageXFile = await imagePicker.pickImage(source: source);
    final File imageFile = File(imageXFile!.path);
    return imageFile.path;
  }
}