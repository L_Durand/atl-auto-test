import 'package:artosage/models/conversation.dart';
import 'package:artosage/models/message.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/conversation_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/conversation_page.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';

import '../service/storage_service.dart';

class ConversationList extends StatefulWidget {
  static String routeName = '/conversations';

  UserService userService = UserService();
  ConversationService conversationService = ConversationService();
  String? token;

  ConversationList({super.key});

  @override
  State<ConversationList> createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  final StorageService storage = StorageService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
        future: getContent(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return CustomScaffold(
                const Center(
                    child: CircularProgressIndicator(
                        color: GlobalStyle.primaryColor)), true, true, 0, false);
          } else {
            return CustomScaffold(
                _buildConversationsList(widget.conversationService.conversations),
                true,
                true,
                2,
                false
            );
          }
        });
  }
  
  Widget _buildConversationsList(List<Conversation> conversations) {
    return ListView.builder(
      itemBuilder: ((context, index) => _buildConversation(conversations[index])),
      itemCount: conversations.length,
      shrinkWrap: true,
    );
  }
  
  Widget _buildConversation(Conversation conversation) {
    User interlocutor;
    Message lastMessage = conversation.messages[conversation.messages.length-1];
    String lastMessageAuthorText = lastMessage.author.id == widget.userService.activeUser.id ? "Vous: " : "";
    conversation.creator.id == widget.userService.activeUser.id ? interlocutor = conversation.otherUser : interlocutor = conversation.creator;
    var myToken;
    if (widget.token == null){
      myToken = '';
    } else {
      myToken = widget.token;
    }
    return InkWell(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ConversationPage(conversation, widget.userService.activeUser, myToken))),
      child: ConversationListUserWidget(interlocutor, true, false, false, false, false, false, widget.conversationService.conversations, widget.token, widget.userService.activeUser, lastMessageAuthorText + lastMessage.content),
    );
  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    User actualUser = await widget.userService.fetchUser(widget.token);
    List users = await widget.userService.getAllUsers(widget.token);
    List<Conversation> conversations = await widget.conversationService.getAllConversations(
        widget.token, users);
    return [actualUser, conversations];
  }

}