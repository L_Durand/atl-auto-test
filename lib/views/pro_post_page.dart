import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';

class ProPostPage extends StatefulWidget {
  static String routeName = '/pro_post';
  final Post post;
  final String? token;
  
  const ProPostPage(this.post, this.token, {super.key});
  
  @override
  State<StatefulWidget> createState() => _CivilPostPageState();
}

class _CivilPostPageState extends State<ProPostPage> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      ListView(
          children: [
            PostWidget(HardData.actualUser, widget.post, true, widget.token, []),
          ],
        ),
      true, 
      false, 
      0,
      false
    );
  }
}