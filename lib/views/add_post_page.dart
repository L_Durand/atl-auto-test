import 'package:artosage/models/location.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/post_service.dart';
import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class AddPostPage extends StatefulWidget {
  static String routeName = '/add_post';
  const AddPostPage({super.key});

  @override
  State<AddPostPage> createState() => _AddPostPageState();
}

class _AddPostPageState extends State<AddPostPage> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      AddPostWidget(), true, true, 2, true
    );
  }
}

// ignore: must_be_immutable
class AddPostWidget extends StatefulWidget {
  UserService userService = UserService();
  PostService postService = PostService();
  String? token;
  String? selectedImageName = "Pas d'image sélectionnée";
  
  AddPostWidget({super.key});

  @override
  State<AddPostWidget> createState() => _AddPostWidgetState();
}

class _AddPostWidgetState extends State<AddPostWidget> {
  OutlineInputBorder inputBorder = OutlineInputBorder(
    borderSide: const BorderSide(
      color: GlobalStyle.inputGrey
    ),
    borderRadius: BorderRadius.circular(8.0),
  );

  List<DropdownMenuItem<PostTypeEnum>> dropDownItems= <DropdownMenuItem<PostTypeEnum>>[
    DropdownMenuItem(value: PostTypeEnum.ad, child: Text(PostTypeEnum.ad.value,)),
    DropdownMenuItem(value: PostTypeEnum.adviceRequest, child: Text(PostTypeEnum.adviceRequest.value)),
    DropdownMenuItem(value: PostTypeEnum.advice, child: Text(PostTypeEnum.advice.value)),
  ];

  StorageService storage = StorageService();
  List<PlatformFile> selectedFiles = [];
  PostTypeEnum selectedType = PostTypeEnum.ad;
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  late List<String> imageFiles;
  Position? _currentPosition;

  @override
  initState(){
    super.initState();
    selectedType = PostTypeEnum.ad;
    imageFiles=[];
  }
  
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
      future: getContent(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          // Si le Future est en attente, afficher une indication de chargement
          return const Center(
            child: CircularProgressIndicator(
            color: GlobalStyle.primaryColor,)
          );
        } else {
          if ((widget.userService.activeUser.type == UserTypeEnum.civil) &&
              (dropDownItems.length > 2)) {
            dropDownItems.removeAt(2);
          }
          // Si le Future a réussi, récupérer la valeur booléenne et l'afficher
          return _buildAddPost(context, widget.token, widget.userService.activeUser.id);
        }
    });
  }

  Widget _buildAddPost(context, token, idUser){
    final _formKey = GlobalKey<FormState>();

    return SizedBox(
      height: 563,
      child: Column(
        children: [
          Expanded(
            child: Form(
              key: _formKey,
              child: Container(
                padding: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    _buildTextField(
                      titleController, 
                      "Entrez le titre de votre annonce..", 
                      "Veuillez entrer un titre pour votre publication.", 
                      1
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: DropdownButtonFormField(
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          isDense: true,
                          contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                          enabledBorder: inputBorder,
                          focusedBorder: inputBorder,
                          hintText: "Type d'annonce..",
                          hintStyle: GlobalStyle.interRegular
                            .copyWith(color: GlobalStyle.inputGrey)
                            .copyWith(fontSize: 15),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide(
                              color: GlobalStyle.inputGrey
                            ),
                          ),
                        ),
                        items: dropDownItems, 
                        value: selectedType,
                        onChanged: (PostTypeEnum? newValue){
                          setState(() {
                            selectedType = newValue!;
                          });
                        },
                      ),
                    ),
                    _buildTextField(
                      descriptionController, 
                      "Écrire une annonce..", 
                      "Veuillez entrer du texte pour votre publication.", 
                      10
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: TextFormField(
                        readOnly: true,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          isDense: true,
                          contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                          enabledBorder: inputBorder,
                          focusedBorder: inputBorder,
                          hintText: widget.selectedImageName,
                          hintStyle: GlobalStyle.interRegular
                            .copyWith(color: GlobalStyle.inputGrey)
                            .copyWith(fontSize: 15),
                          border: const OutlineInputBorder(
                            borderSide: BorderSide(
                              color: GlobalStyle.inputGrey
                            ),
                          ),
                          suffixIcon: IconButton(
                            icon: const Icon(Icons.photo_size_select_actual_rounded),
                            color: GlobalStyle.thirdColor,
                            onPressed: () async => {
                              await _selectFile(),
                            },
                          ),
                        ),
                        validator: (value) {
                          return null;
                        },
                      ),
                    ),
                    // CustomValidationButton(
                    //   text: "Trouver ma localisation (optionnel)",
                    //   onPressed: (){
                    //     _getCurrentPosition();
                    //   },
                    // ),
                    CustomValidationButton(
                      text: 'Poster',
                      horizontalMargin: 0,
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState?.save();

                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('En cours de publication...')),
                          );

                          widget.postService.saveNewPost(context, token, idUser.toString(), typeEnum(selectedType), descriptionController.text, selectedFiles);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String typeEnum(typeId) {
    String type;
    switch (typeId) {
      case PostTypeEnum.ad:
        type = '1';
        break;
      case PostTypeEnum.advice:
        type = '2';
        break;
      default:
        type = '3';
    }
    return type;
  }

  Widget _buildTextField(TextEditingController controller, String placeholder, String errorText, int linesAmount){
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: TextFormField(
        controller: controller,
        minLines: linesAmount,
        maxLines: linesAmount,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return errorText;
          }
          return null;
        },
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          isDense: true,
          contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
          enabledBorder: inputBorder,
          focusedBorder: inputBorder,
          hintText: placeholder,
          hintStyle: GlobalStyle.interRegular
            .copyWith(color: GlobalStyle.inputGrey)
            .copyWith(fontSize: 15),
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: GlobalStyle.inputGrey
            ),
          ),
        ),
      ),
    );
  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    User actualUser = await widget.userService.fetchUser(widget.token);
    return [actualUser];
  }

  _selectFile() async {
    // Lets the user pick one file; files with any file extension can be selected
    FilePickerResult? result =
    await FilePicker.platform.pickFiles(type: FileType.any);

    // The result will be null, if the user aborted the dialog
    if (result?.files != null) {
      result!.files.forEach((element) {
        setState(() {
          selectedFiles.add(element);
        });
      });
          _buildListView();
    }}

  _buildListView() {
    List list = [];
    if (selectedFiles.isNotEmpty) {
      for(var name in selectedFiles){
        list.add(name.name);
      }
      widget.selectedImageName = list.toString();
    }
    return const Text('No files selected');
  }

  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Le service de géolocalisation est désactivé. Veuillez l'activer pour profiter de cette fonctionnalité.")
      ));
      return false;
    }
    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('La géolocalisation a été refusée pour le moment.'))
        );
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('La géolocalisation a été refusée pour toujours.')));
      return false;
    }  

    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();  
    widget.userService.activeUser.isAddressShareAuthorized = hasPermission;
    if (!hasPermission) return;

    await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    ).then((Position position){
      setState(() {
        _currentPosition = position;
    });
      widget.userService.activeUser.location = Location(_currentPosition!.latitude, _currentPosition!.longitude);
      print(widget.userService.activeUser.location);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('La géolocalisation a été récupérée.'))
      );
    }).catchError((e){
      print(e);
    });
  }
}