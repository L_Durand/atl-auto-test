import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:heroicons/heroicons.dart';
import '../share/global_style.dart';

class SearchPage extends StatefulWidget {
  static String routeName = '/search';
  final List<User> _users = HardData.users;
  // final List<Post> _posts = HardData.civilPosts;
  final bool isInput;
  final TextEditingController? foreignController;

  SearchPage(this.isInput, this.foreignController, {super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late TextEditingController _searchController = TextEditingController();
  late bool isResultsVisible = false;
  late List<User> results = [];

  @override
  void initState() {
    super.initState();

    isResultsVisible = false;

    if (!widget.isInput && widget.foreignController != null){
      _searchController = widget.foreignController!;
      _onSearch();
    }
  }

  OutlineInputBorder searchInputBorder = OutlineInputBorder(
    borderSide: const BorderSide(
      color: GlobalStyle.inputGrey
    ),
    borderRadius: BorderRadius.circular(8.0),
  );

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(_buildSearchWidget() , true, true, 3, false);
  }

  _buildSearchWidget(){
    return Column(
      children: [
        Visibility(
          visible: widget.isInput,
          child: Container(
            padding: const EdgeInsets.all(16),
            child: TextField(
              controller: _searchController,
              cursorColor: GlobalStyle.primaryColor,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                enabledBorder: searchInputBorder,
                focusedBorder: searchInputBorder,
                hintText: "Recherche...",
                hintStyle: GlobalStyle.interRegular
                  .copyWith(color: GlobalStyle.inputGrey)
                  .copyWith(fontSize: 15),
                prefixIcon: IconButton(
                  padding: const EdgeInsets.all(4),
                  icon: const HeroIcon(
                    HeroIcons.magnifyingGlass, 
                    style: HeroIconStyle.solid,
                    color: GlobalStyle.secondaryColor,
                    size: 30,
                  ),
                  onPressed: (){
                    _onSearch();
                  },
                ),
                suffixIcon: IconButton(
                  icon: const Icon(Icons.clear),
                  color: GlobalStyle.secondaryColor,
                  splashColor: GlobalStyle.primaryColor,
                  onPressed: () => {
                    _searchController.clear(),
                    setState(() {
                      isResultsVisible=false;  
                    }),
                  },
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: isResultsVisible,
          child: Expanded(
            child: ListView.builder(
              itemCount: results.length,
              itemBuilder: (BuildContext context, int index) => _buildResult(results[index]),
            ),
          ),
        ),
      ],
    );
  }

  _onSearch(){
    results = widget._users.where((user) => user.name.toLowerCase().contains(_searchController.text.toLowerCase())).toList();

    setState(() {
      isResultsVisible = _searchController.text != "" ? true : false;
    });
  }

  Widget _buildResult(User user){
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 1.0),
      child: Row(
        children: [
          // PostHeaderWidget(user, true, true, false, false, false, false),
        ],
      ),
    );
  }
}