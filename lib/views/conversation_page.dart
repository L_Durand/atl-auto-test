import 'package:artosage/models/conversation.dart';
import 'package:artosage/models/message.dart';
import 'package:artosage/service/conversation_service.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:flutter/material.dart';
import 'package:heroicons/heroicons.dart';
import '../models/user.dart';
import '../share/custom_scaffold.dart';

// ignore: must_be_immutable
class ConversationPage extends StatefulWidget {
  Conversation conversation;
  User actualUser;
  String token;

  ConversationService conversationService = ConversationService();
  ConversationPage(this.conversation, this.actualUser, this.token, {super.key});

  @override
  State<ConversationPage> createState() => _ConversationPageState();
}

class _ConversationPageState extends State<ConversationPage> {
  TextEditingController textController = TextEditingController();
  ScrollController scrollController = ScrollController();

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      scrollController.jumpTo(scrollController.position.maxScrollExtent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      _buildPage(),
      true,
      true,
      2,
      false
    );
  }
  
  Widget _buildPage() {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.all(20),
      child: ListView(
        controller: scrollController,
        shrinkWrap: true,
        children: [
          ListView.builder(
            shrinkWrap: true,
            itemCount: widget.conversation.messages.length,
            itemBuilder: (BuildContext context, int index) => _buildMessage(widget.conversation.messages[index]),
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            child: Material(
              elevation: 1.0,
              borderRadius: const BorderRadius.all(Radius.circular(15)),
              child: TextField(
                controller: textController,
                cursorColor: GlobalStyle.primaryColor,
                onSubmitted: ((value) => _addMessage(textController)),
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: const HeroIcon(HeroIcons.paperAirplane),
                    color: GlobalStyle.secondaryColor,
                    onPressed: () async => {
                      print("message: ${textController.text}"),
                      _addMessage(textController)
                    },
                  ),
                  filled: true,
                  fillColor: GlobalStyle.backgroundColor,
                  isDense: true,
                  contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                  border: InputBorder.none,
                  hintText: "Votre message",
                  hintStyle: GlobalStyle.interRegular
                    .copyWith(color: GlobalStyle.secondaryColor)
                    .copyWith(fontSize: 15),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildMessage(Message message) {
    bool isAuthorActualUser = message.author.id == widget.actualUser.id;
    // scrollController.jumpTo(scrollController.position.maxScrollExtent);

    List<Widget> childrenWidgets = [
      Container(
        alignment: Alignment.topCenter,
        child: CircleAvatar(
            backgroundImage: AssetImage(message.author.profilePicFile),
          ),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 6),
        child: Column(
          crossAxisAlignment: isAuthorActualUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 6, 
                horizontal: 10,
              ),
              decoration: BoxDecoration(
                color: isAuthorActualUser ? GlobalStyle.secondaryColor : GlobalStyle.primaryColor, 
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              constraints: const BoxConstraints(maxWidth: 280),
              child: Text(
                message.content,
                style: GlobalStyle.interRegular
                  .copyWith(fontSize: 15)
                  .copyWith(color: Colors.white)
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 2),
              child: Text(
                '${message.author.firstName} ${message.author.name}',
                  style: GlobalStyle.interRegular
                    .copyWith(fontSize: 13)
                    .copyWith(color: isAuthorActualUser ? GlobalStyle.secondaryColor : GlobalStyle.primaryColor),
                ),            
            ),
          ],
        ),
      ), 
    ];

    return Material(
      child: Container(
        color: Colors.white,
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
          mainAxisAlignment: isAuthorActualUser ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: isAuthorActualUser ? childrenWidgets.reversed.toList() : childrenWidgets,
        ),
      ),
    );
  }

  void _addMessage(TextEditingController controller){
    if(controller.text != ""){
      setState(() {
        widget.conversationService.addMessage(controller.text, '${widget.conversation.otherUser.id}', widget.token, widget.conversation, context, widget.actualUser);
        widget.conversation.messages.add(Message(widget.actualUser, controller.text, DateTime.now()));
        controller.clear();
        scrollController.jumpTo(scrollController.position.maxScrollExtent);
      });
    }
  }
}