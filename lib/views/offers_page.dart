import 'package:artosage/models/conversation.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';
import 'civil_post_page.dart';

class OffersPage extends StatefulWidget {
  static String routeName = '/offers';
  const OffersPage({super.key});

  @override
  State<OffersPage> createState() => _OffersPageState();
}

class _OffersPageState extends State<OffersPage> {
  @override
  Widget build(BuildContext context) {
    final List<Post> userAdPostsWOffers = [];
    // HardData.civilPosts.where((i) => i.author == HardData.actualUser && i.type == PostTypeEnum.ad && i.offers.isNotEmpty).toList();

    return CustomScaffold(
      userAdPostsWOffers.isNotEmpty ?
      ListView.builder(
          itemCount: userAdPostsWOffers.length,
          itemBuilder: ((context, index) => _buildPostOffers(userAdPostsWOffers[index])),
      ) :
      _buildBoldText("Vous n'avez pas de proposition de gardiennage."),
      true,
      false,
      0,
      false
    );
  }

  Widget _buildPostOffers(Post post) {
    String date = '${post.createdAt.day.toString().padLeft(2, '0')}/${post.createdAt.month.toString().padLeft(2, '0')}/${post.createdAt.year}';

    return Column(
      children: [
        PostInkwell(
          post,
          _buildBoldText('${post.title} (annonce du $date) : '),
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: post.offers.length,
          itemBuilder: ((context, index) => UserOfferBannerWidget(post.offers[index], true, true, false, true, post, () => _refreshWidget(), false, false, [],'',HardData.actualUser)),
        ),
      ],
    );
  }

  Widget _buildBoldText(String text) {
    return Container(
      alignment: Alignment.topLeft,
      margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
      child: Text(
        text, 
        style: GlobalStyle.interBold
          .copyWith(color: GlobalStyle.secondaryColor)
          .copyWith(fontSize: 14)
      ),
    );
  }

  _refreshWidget() {
    setState(() {
    });
  }
}

class PostInkwell extends StatelessWidget {
  final Widget child;
  final Post post;

  const PostInkwell(this.post, this.child, {super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => openPostPage(context),
      child: child,
    );
  }

  void openPostPage(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CivilPostPage(post, HardData.actualUser, '')));
  }
}