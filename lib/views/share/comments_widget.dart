import 'package:artosage/models/comment.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/post_service.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/views/civil_post_page.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:heroicons/heroicons.dart';

class CommentsWidget extends StatefulWidget {
  final Post post;
  final bool isAllComments;
  final User actualUser;
  final String? token;

  const CommentsWidget(this.post, this.isAllComments, this.actualUser, this.token,{super.key});

  @override
  State<CommentsWidget> createState() => _CommentsWidgetState();
}

class _CommentsWidgetState extends State<CommentsWidget> {
  bool isCommentClicked = false;

  @override
  void initState(){
    super.initState();
    
    isCommentClicked = widget.isAllComments;
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isAllComments){
      return Column(
        children: [
          _buildCommentsAmount(),
          SimpleCommentsWidget(widget.actualUser, widget.post, isCommentClicked, widget.isAllComments, widget.token),
        ]
      );
    }

    return SimpleCommentsWidget(widget.actualUser, widget.post, isCommentClicked, widget.isAllComments, widget.token);
  }

  _buildCommentsAmount(){
    String commentWord = widget.post.comments.length == 1 ? 'commentaire' : 'commentaires';

    return Visibility(
      visible: widget.post.comments.isNotEmpty,
      child: Container(
        margin: const EdgeInsets.fromLTRB(20.0, 7.0, 15.0, 5.0),
        child: InkWell(
          onTap: (){
            setState(() {
              isCommentClicked = !isCommentClicked;
            });
          },
          child: Container(
            alignment: Alignment.centerRight,
            child: Text(
              '${widget.post.comments.length} $commentWord', 
              style: GlobalStyle.interBold.copyWith(
                color: GlobalStyle.secondaryColor
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SimpleCommentsWidget extends StatefulWidget {
  final Post post;
  final bool isCommentsClicked;
  final bool isAllComments;
  final User actualUser;
  PostService postService = PostService();
  String? token;

  SimpleCommentsWidget(this.actualUser, this.post, this.isCommentsClicked, this.isAllComments, this.token, {super.key});

  @override
  State<SimpleCommentsWidget> createState() => _SimpleCommentsWidgetState();
}

class _SimpleCommentsWidgetState extends State<SimpleCommentsWidget> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.isAllComments || widget.isCommentsClicked,
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            ListView.builder(
              shrinkWrap: true,
              itemCount: widget.isAllComments ? widget.post.comments.length : widget.post.comments.length < 2 ? 1 : 2,
              itemBuilder: (BuildContext context, int index) => _buildCommentItem(widget.post.comments[index]),
            ),
            Visibility(
              visible: !widget.isAllComments,
              child: _buildOpenPageBtn(context),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
              child: Material(
                elevation: 1.0,
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                child: TextField(
                  controller: controller,
                  cursorColor: GlobalStyle.primaryColor,
                  onSubmitted: ((value) => _addComment(controller)),
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: const HeroIcon(HeroIcons.paperAirplane),
                      color: GlobalStyle.secondaryColor,
                      onPressed: () async => {
                        await widget.postService.addComment(controller.text, widget.post.id.toString(), widget.token, context)
                      },
                    ),
                    filled: true,
                    fillColor: GlobalStyle.backgroundColor,
                    isDense: true,
                    contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                    border: InputBorder.none,
                    hintText: "Votre commentaire",
                    hintStyle: GlobalStyle.interRegular
                      .copyWith(color: GlobalStyle.secondaryColor)
                      .copyWith(fontSize: 15),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCommentItem(Comment comment){
    bool isAuthorActualUser = comment.user.id == widget.actualUser.id;

    List<Widget> childrenWidgets = [
      Container(
        alignment: Alignment.topCenter,
        child: ProfileInkwell(
          CircleAvatar(
            backgroundImage: AssetImage(comment.user.profilePicFile),
          ),
          comment.user,
          true
        ),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 6),
        child: Column(
          crossAxisAlignment: isAuthorActualUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 6, 
                horizontal: 10,
              ),
              decoration: BoxDecoration(
                color: isAuthorActualUser ? GlobalStyle.secondaryColor : GlobalStyle.primaryColor, 
                borderRadius: const BorderRadius.all(Radius.circular(10)),
              ),
              constraints: const BoxConstraints(maxWidth: 280),
              child: Text(
                comment.content,
                style: GlobalStyle.interRegular
                  .copyWith(fontSize: 15)
                  .copyWith(color: Colors.white)
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 2),
              child: ProfileInkwell(
                Text(
                  '${comment.user.firstName} ${comment.user.name}',
                  style: GlobalStyle.interRegular
                    .copyWith(fontSize: 13)
                    .copyWith(color: isAuthorActualUser ? GlobalStyle.secondaryColor : GlobalStyle.primaryColor),
                ),
                comment.user,
                true
              ),              
            ),
          ],
        ),
      ), 
    ];

    return Material(
      child: Container(
        color: Colors.white,
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
          mainAxisAlignment: isAuthorActualUser ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: isAuthorActualUser ? childrenWidgets.reversed.toList() : childrenWidgets,
        ),
      ),
    );
  }

  Widget _buildOpenPageBtn(BuildContext context){
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.fromLTRB(1, 2, 1, 6),
      child: InkWell(
        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CivilPostPage(widget.post, widget.actualUser, widget.token))),
        child: Text(
          'Afficher tous les commentaires', 
          style: GlobalStyle.interBold.copyWith(
            color: GlobalStyle.secondaryColor
          ),
        ),
      ),
    );
  }

  void _addComment(TextEditingController controller){
    if(controller.text != ""){
      setState(() {
        widget.post.comments.add(Comment(3, controller.text, DateTime.now(), null, widget.actualUser));
        controller.clear();
      });
    }
  }
}