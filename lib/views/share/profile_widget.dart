import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:badges/badges.dart' as Badge;
import 'package:flutter/material.dart';

class ProfileWidget extends StatefulWidget {
  final User profileUser;
  final User actualUser;
  final List posts;
  final List conversation;
  final String? token;

  ProfileWidget(this.profileUser, this.actualUser, this.posts, this.conversation, this.token, {super.key});

  @override
  State<ProfileWidget> createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  @override
  Widget build(BuildContext context) {
    List userPosts = widget.posts.where((i) => i.author.id == widget.profileUser.id).toList();
    return Column(
      children: [
        _buildBanner(),
        UserProfileBannerWidget(widget.profileUser, true, false, false, false, false, true, widget.conversation, widget.token, widget.actualUser),
        Expanded(
          child: PostListWidget(widget.actualUser, userPosts, widget.conversation, widget.token),
        ),
      ],
    );
  }

  _buildBanner(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 100),
          child: Image.asset(
            widget.profileUser.bannerPicFile,
            fit: BoxFit.fitWidth,
            alignment: Alignment.center,
          ),
        ),
      ],
    );
  }

  _buildOptions(){
    return null;
  }
}

class RoleBadge extends StatelessWidget {
  final String text;
  final bool isBadgeUnder;

  const RoleBadge(this.text, this.isBadgeUnder, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3, horizontal: isBadgeUnder ? 0 : 5),
      child: Badge.Badge(
        badgeContent: Text(
            text.toUpperCase(),
            style: GlobalStyle.interSemiBold
              .copyWith(fontSize: 10)
              .copyWith(color: GlobalStyle.primaryColor)
        ),
        badgeStyle: Badge.BadgeStyle(
          badgeColor: GlobalStyle.transparentPrimaryColor,
          borderRadius: BorderRadius.circular(6),
          shape: Badge.BadgeShape.square,
          padding: const EdgeInsets.all(4.0),
        ),
      ),
    );  
  }
}