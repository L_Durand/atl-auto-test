import 'package:artosage/models/user.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/civil_post_list.dart';
import 'package:artosage/views/conversation_list.dart';
import 'package:artosage/views/location_page.dart';
import 'package:artosage/views/offers_page.dart';
import 'package:artosage/views/pro_post_list.dart';
import 'package:artosage/views/search_page.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:heroicons/heroicons.dart';
import '../../share/global_style.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class HeaderWidget extends StatefulWidget {
  User actualUser = HardData.actualUser;

  HeaderWidget({super.key});

  @override
  State<HeaderWidget> createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  late Widget logo = InkWell(
    child: Text(
      "A'Rosa-Je",
      style: GlobalStyle.pacificoRegularWhiteStyle,
    ),
    onTap: () => Navigator.pushNamedAndRemoveUntil(context, "/civil_posts", (route) => false)
  );
  final TextEditingController _searchController = TextEditingController();
  OutlineInputBorder searchInputBorder = OutlineInputBorder(
    borderSide: const BorderSide(
      color: GlobalStyle.inputGrey,
    ),
    borderRadius: BorderRadius.circular(25.0),
  );

  @override
  Widget build(BuildContext context){
    bool isMobileIconVisible = ModalRoute.of(context)!.settings.name != "/conversations" && ModalRoute.of(context)!.settings.name != "/conversation" && ModalRoute.of(context)!.settings.name != "/offers" && ModalRoute.of(context)!.settings.name != "/settings" && ModalRoute.of(context)!.settings.name != "/manage_account";

    if (MediaQuery.of(context).size.width >= 1300){
      return _buildAppBar(
        Container(
          width: 150,
          padding: const EdgeInsets.only(left: 20),
          child: logo,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildAppBarItem(
              HeroIcons.chatBubbleLeftEllipsis,
              "Annonces",
              CivilPostList.routeName,
            ),
            _buildAppBarItem(
              HeroIcons.lightBulb,
              "Conseils",
              ProPostList.routeName,
            ),
            SizedBox(
              width: 250,
              height: 35,
              child: TextField(
                controller: _searchController,
                cursorColor: GlobalStyle.primaryColor,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  enabledBorder: searchInputBorder,
                  focusedBorder: searchInputBorder,
                  hintText: "Recherche...",
                  hintStyle: GlobalStyle.interRegular
                    .copyWith(color: GlobalStyle.inputGrey)
                    .copyWith(fontSize: 15),
                  prefixIcon: IconButton(
                    padding: const EdgeInsets.all(4),
                    icon: const HeroIcon(
                      HeroIcons.magnifyingGlass, 
                      style: HeroIconStyle.solid,
                      color: GlobalStyle.secondaryColor,
                      size: 30,
                    ),
                    onPressed: (){
                      if(_searchController.text.isNotEmpty){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SearchPage(false, _searchController)));
                      }
                    },
                  ),
                  suffixIcon: IconButton(
                    padding: const EdgeInsets.all(4),
                    icon: const HeroIcon(
                      HeroIcons.xMark, 
                      size: 30,
                      color: GlobalStyle.secondaryColor,
                    ),
                    splashColor: GlobalStyle.primaryColor,
                    onPressed: () => {
                      _searchController.clear(),
                    },
                  ),
                ),
              ),
            ),
            _buildAppBarItem(
              HeroIcons.map,
              "Carte",
              LocationPage.routeName,
            ),
            _buildAppBarItem(
              HeroIcons.bell, 
              "Offres",
              OffersPage.routeName,
            ),
          ],
        ),
        [
          Container(
            alignment: Alignment.centerRight,
            width: 207,
            height: 50,
            padding: const EdgeInsets.fromLTRB(0, 4, 20, 4),
            child: ProfileInkwell(
              const HeroIcon(
                HeroIcons.user, 
                style: HeroIconStyle.outline,
                size: 35
              ),
              widget.actualUser,
              true
            ),
          ),
        ],
      );
    }
    else {
      return _buildAppBar(
        null,
        logo,
        [
          Visibility(
            visible: isMobileIconVisible,
            child: IconButton(
              onPressed: () => onBellPressed(context),
              icon: const HeroIcon(
                HeroIcons.bell,
                color: Colors.white,
              ),
            ),
          ),
          Visibility(
            visible: isMobileIconVisible,
            child: IconButton(
              onPressed: () => onMessagePressed(context),
              icon: const HeroIcon(
                HeroIcons.chatBubbleLeft,
                color: Colors.white,
              ),
            ),
          ),
        ]
      );
    }
  }

  Widget _buildAppBar(Widget? leading, Widget title, List<Widget> actions){
    return AppBar(
      leading: leading,
      leadingWidth: leading != null ? 175 : 50,
      title: title,
      actions: actions,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: const AssetImage('assets/images/background.png'),
            repeat: ImageRepeat.repeat,
            scale: 3,
            colorFilter: ColorFilter.mode(
              GlobalStyle.secondaryColor.withOpacity(0.8),
              BlendMode.darken,
          ),
          ),

        ),
      ),
      backgroundColor: GlobalStyle.secondaryColor,
      toolbarHeight: MediaQuery.of(context).size.width >= 1300 ? 70 : 50,
      centerTitle: true,
    );
  }

  _buildAppBarItem(HeroIcons icon, String label, String route){
    Color color = Colors.white;
    
    if(ModalRoute.of(context)!.settings.name == route){
      color = GlobalStyle.primaryColor;
    }

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          IconButton(
            icon: HeroIcon(
              icon,
              color: color,
              size: 60,
            ),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(context, route, (route) => false);
            },
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 6),
            child: Text(
              label,
              style: GlobalStyle.interRegular
                .copyWith(fontSize: 13)
                .copyWith(color: color)
            ),
          ),   
        ]
      ),
    );
  }

  onBellPressed(BuildContext context){
    Future.delayed(Duration.zero,() {
      Navigator.pushNamed(context, OffersPage.routeName);
    });
  }
  
  onMessagePressed(BuildContext context) {
    Navigator.pushNamed(context, ConversationList.routeName);
  }
}