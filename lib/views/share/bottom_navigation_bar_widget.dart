import 'dart:ui';
import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/views/add_post_page.dart';
import 'package:artosage/views/civil_post_list.dart';
import 'package:artosage/views/location_page.dart';
import 'package:artosage/views/pro_post_list.dart';
import 'package:artosage/views/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:heroicons/heroicons.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  final int selectedIndex;
  const BottomNavigationBarWidget(this.selectedIndex, {super.key});

  @override
  State<BottomNavigationBarWidget> createState() => _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {

  @override
  Widget build(BuildContext context){
    return SizedBox(
      height: 85,
      child: Container(
        height: 85,
        padding: const EdgeInsets.all(7.0),
        alignment: Alignment.center,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 4,
              sigmaY: 4,
            ),
            child: _buildBar(),
          ),
        ),
      ),
    );
  }

  _buildBar(){
    UserService userService = UserService();
    StorageService storage = StorageService();
    
    //PASSER EN CONTAINER
    return Wrap(
      alignment: WrapAlignment.center,
      children: [
        BottomNavigationBar(
          currentIndex: widget.selectedIndex,
          selectedItemColor: GlobalStyle.primaryColor,
          unselectedItemColor: GlobalStyle.secondaryColor,
          backgroundColor: GlobalStyle.transparentWhite,
          type: BottomNavigationBarType.fixed,
          iconSize: 28.0,
          selectedFontSize: 14.0,
          unselectedFontSize: 12.0,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: HeroIcon(
                HeroIcons.chatBubbleLeftEllipsis, 
                style: HeroIconStyle.outline
              ),
              label: 'Annonces'
            ),
            BottomNavigationBarItem(
              icon: HeroIcon(
                HeroIcons.lightBulb, 
                style: HeroIconStyle.outline
              ),
              label: 'Conseils'
            ),
            BottomNavigationBarItem(
              icon: HeroIcon(HeroIcons.plusCircle, 
                style: HeroIconStyle.solid,
                color: GlobalStyle.primaryColor,
                size: 48,
              ),
              label: ''
            ),
            BottomNavigationBarItem(
              icon: HeroIcon(
                HeroIcons.map, 
                style: HeroIconStyle.outline
              ),
              label: 'Carte'
            ),
            BottomNavigationBarItem(
              icon: HeroIcon(
                HeroIcons.user, 
                style: HeroIconStyle.outline
              ),
              label: 'Profil'
            ),
          ],
          onTap: (int index) {
            String page = "/";
            switch (index){
              case 0:
                page = CivilPostList.routeName;

                break;
              case 1:
                page = ProPostList.routeName;

                break;
              case 2:
                page = AddPostPage.routeName;

                break;
              case 3: 
                page = LocationPage.routeName;

                break;
              case 4: 
                page = ProfilePage.routeName;

                break;
            }
            Navigator.pushNamedAndRemoveUntil(context, page, (route) => false);
          },
        ),
      ],
    );
  }
}