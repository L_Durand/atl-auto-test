import 'package:heroicons/heroicons.dart';
import '../../share/global_style.dart';
import 'package:flutter/material.dart';

AppBar header() {
  return AppBar(
    title: Text(
      "A'Rosa-Je",
      style: GlobalStyle.pacificoRegularWhiteStyle,
    ),
    actions: [
      // IconButton(
      //   onPressed: onBellPressed(), 
      //   icon: const HeroIcon(
      //     HeroIcons.bell,
      //     color: Colors.white,
      //   )
      // )
    ],
    flexibleSpace: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: const AssetImage('assets/images/background.png'),
          repeat: ImageRepeat.repeat,
          scale: 3,
          colorFilter: ColorFilter.mode(
            GlobalStyle.secondaryColor.withOpacity(0.8),
            BlendMode.darken,
          ),
        ),
      ),
    ),
    backgroundColor: GlobalStyle.secondaryColor,
    toolbarHeight: 50,
    centerTitle: true,
    // automaticallyImplyLeading: false,
  );
}

onBellPressed() {
  // Navigator.push(HardData.mainContext, MaterialPageRoute(builder: (context) => const OffersPage()));
}