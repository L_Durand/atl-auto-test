// ignore_for_file: must_be_immutable
import 'package:artosage/models/conversation.dart';
import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/add_post_page.dart';
import 'package:artosage/views/civil_post_page.dart';
import 'package:artosage/views/conversation_page.dart';
import 'package:artosage/views/pro_post_page.dart';
import 'package:artosage/views/profile_page.dart';
import 'package:artosage/views/settings_page.dart';
import 'package:artosage/views/share/carousel_widget.dart';
import 'package:artosage/views/share/comments_widget.dart';
import 'package:artosage/views/share/profile_widget.dart';
import 'package:flutter/material.dart';
import 'package:heroicons/heroicons.dart';

class PostListWidget extends StatefulWidget {
  final User actualUser;
  final List posts;
  final List conversation;
  final String? token;

  const PostListWidget(this.actualUser, this.posts, this.conversation, this.token, {super.key});

  @override
  State<PostListWidget> createState() => _PostListWidgetState();
}

class _PostListWidgetState extends State<PostListWidget> {
  late bool isFormVisible;

  @override
  void initState() {
    super.initState();

    isFormVisible = false;
  }
  
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        Visibility(
          visible: MediaQuery.of(context).size.width > 1300 && ModalRoute.of(context)!.settings.name == "/civil_posts" ? true : false,
          child: Column(
            children: [
              Container(),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: GlobalStyle.primaryColor,
                  ),
                  onPressed: (){
                    setState(() {
                      isFormVisible = !isFormVisible;
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        isFormVisible ? "Fermer le formulaire" : "Ouvrir le formulaire pour créer une publication",
                        style: GlobalStyle.interRegular.copyWith(fontSize: 18),
                      ),
                      HeroIcon(
                        isFormVisible ? HeroIcons.arrowUp : HeroIcons.arrowDown,
                        size: 22,
                      ),
                    ]
                  ),
                ),
              ),
              Visibility(
                visible: isFormVisible,
                child: AddPostWidget(),
              ),
            ],
          ),
        ),
        ListView.builder(
          itemCount: widget.posts.length,
          itemBuilder: (BuildContext context, int index) => PostWidget(widget.actualUser, widget.posts[index], false, widget.token, widget.conversation),
          shrinkWrap: true,
        ),
      ],
    );
  }
}

class PostWidget extends StatefulWidget {
  final Post post;
  final User actualUser;
  final bool isFullPage;
  final String? token;
  final List conversation;
  const PostWidget(this.actualUser, this.post, this.isFullPage, this.token, this.conversation, {super.key});

  @override
  State<PostWidget> createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  @override
  Widget build(BuildContext context){
    return _buildPost(context);
  }

  _buildPost(BuildContext context){
    List<Widget> civilWidgets = [
      Container(
        margin: const EdgeInsets.only(top: 7.5),
        child: PostHeaderWidget(widget.post.author, true, true, widget.post.type==PostTypeEnum.ad && widget.post.author.isAddressShareAuthorized ? true : false, false, false, false, widget.conversation, widget.token, widget.actualUser),
      ),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        child: Column(
          children: [
            _buildPostTitle(),
            _buildCivilPostText(),
          ],
        ),
      ),
      _buildPostSlider(widget.post),
      widget.isFullPage ? Container() : CommentsWidget(widget.post, false, widget.actualUser, widget.token),
      Container(
        margin: const EdgeInsets.only(top: 8.0),
        child: _buildPostBottom(),
      ),
    ];

    List<Widget> proWidgets = [
      _buildPostSlider(widget.post),
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        child: Column(
          children: [
            _buildPostTitle(),
            _buildProPostText(),
          ],
        ),
      ),
      widget.isFullPage ? PostHeaderWidget(widget.post.author, false, true, false, true, true, false, widget.conversation, widget.token, widget.actualUser) : Container(),
    ];

    return Column(
      children: widget.post.type == PostTypeEnum.advice ? proWidgets : civilWidgets,
    );
  }

  _buildPostTitle(){
    return Container(
      margin: widget.post.type == PostTypeEnum.advice ? const EdgeInsets.fromLTRB(0, 10, 0, 3) : const EdgeInsets.only(bottom: 3),
      alignment: Alignment.centerLeft,
      child: Text(
        widget.post.type == PostTypeEnum.advice ? widget.post.title : widget.post.type.value,
        style: GlobalStyle.interBold
          .copyWith(fontSize: 15)
          .copyWith(color: widget.post.type == PostTypeEnum.advice ? GlobalStyle.primaryColor :  GlobalStyle.secondaryColor),
      ),
    );
  }

  _buildCivilPostText(){
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        widget.post.description,
        style: GlobalStyle.interRegular
          .copyWith(fontSize: 15)
          .copyWith(color: Colors.black)
      ),
    );
  }

  _buildProPostText(){
    String month = widget.post.createdAt.month.toString().padLeft(2, '0');
    String date = '${widget.post.createdAt.day}/$month/${widget.post.createdAt.year}';

    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            "${widget.post.description}\n \nLe $date",
            maxLines: widget.isFullPage ? null : 2,
            style: GlobalStyle.interRegular
              .copyWith(fontSize: 15)
              .copyWith(color: Colors.black)
          ),
        ),
        Visibility(
          visible: !widget.isFullPage,
          child: Container(
            margin: const EdgeInsets.only(top: 14),
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () => _openProPostPage(),
              child: Text(
                "Lire la suite →",
                style: GlobalStyle.interBold
                  .copyWith(fontSize: 15)
                  .copyWith(color: GlobalStyle.primaryColor)
                  .copyWith(decoration: TextDecoration.underline)
              ),
            ),
          ),
        )
      ],
    );
  }

  _openProPostPage(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ProPostPage(widget.post, widget.token)));
  }

  _buildPostSlider(Post post) {
    var path = post.picPaths;
    List<Image> pics = [];
    if (path != null) {
      for (var element in path) {
        pics.add(Image.network(element.toString()));
      }
    }

    if (pics.isNotEmpty) {
      return Container(
        margin:
        post.type == PostTypeEnum.advice ? const EdgeInsets.only(top: 15) : null,
        child: CarouselWidget(pics),
      );
    } else {
      return Container(
      );
    }
  }

  _buildPostBottom(){
    final textStyle = GlobalStyle.interRegular.copyWith(fontSize: 14);
    bool showOfferBtn = false;
    bool showCommentBtn = !widget.isFullPage;

    if((widget.post.author != widget.actualUser && widget.post.type == PostTypeEnum.ad)) {
      showOfferBtn = true;
    }

    return Row(
      children: [
        Visibility(
          visible: showOfferBtn,
          child: SizedBox(
            height: 35.0,
            width: _getProposeBtnWidth(widget.isFullPage),
            child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(GlobalStyle.primaryColor),
                shape: MaterialStateProperty.all<OutlinedBorder>(
                  const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.zero)
                  ),
                ),
              ),
              onPressed: () => onProposePress(),
              child: Text(
                'Se proposer',
                style: textStyle,
              ),
            ),
          ),
        ),
        Visibility(
          visible: showCommentBtn,
          child: Container(
            height: 35.0,
            width: _getCommentBtnWidth(showOfferBtn),
            decoration: BoxDecoration(
              border: Border.symmetric(
                horizontal: const BorderSide(
                  strokeAlign: BorderSide.strokeAlignInside,
                  width: 1,
                  color: GlobalStyle.primaryColor
                ),
                vertical: BorderSide(
                  // strokeAlign: StrokeAlign.inside,
                  width: MediaQuery.of(context).size.width >= 576 ? 1 : 0,
                  color: GlobalStyle.primaryColor,
                  style: MediaQuery.of(context).size.width >= 576 ? BorderStyle.solid : BorderStyle.none,
                ),
              )
            ),
            child: TextButton(
              // onPressed: () => openCivilPostPage(post),
              onPressed: () => openCivilPostPage(),
              child: Text(
                'Commenter',
                style: textStyle.copyWith(color: GlobalStyle.primaryColor),
              ),
            ),
          ),
        ),
      ],
    );
  }

  double _getProposeBtnWidth(bool isFullPage){
    late double width;
    double smallWidth = 576;
    double midWidth = 768;
    double largeWidth = 992;

    if (MediaQuery.of(context).size.width <= smallWidth){
      width = isFullPage ? MediaQuery.of(context).size.width : MediaQuery.of(context).size.width/2;
    }
    else {
      width = isFullPage ? smallWidth : smallWidth/2;
    }

    return width;
  }

  double _getCommentBtnWidth(bool showOfferBtn){
    late double width;
    double smallWidth = 576;
    double midWidth = 768;
    double largeWidth = 992;

    if (MediaQuery.of(context).size.width <= smallWidth){
      width = showOfferBtn ? MediaQuery.of(context).size.width/2 : MediaQuery.of(context).size.width;
    }
    else {
      width = showOfferBtn ? smallWidth /2 : smallWidth;
    }

    return width;
  }

  void openCivilPostPage(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => CivilPostPage(widget.post, widget.actualUser, widget.token)));
  }

  void onProposePress(){
    if(!widget.post.offers.contains(widget.actualUser)){
      widget.post.offers.add(widget.actualUser);
    }
  }
}

class ProfileInkwell extends StatelessWidget {
  final Widget child;
  final User profileUser;
  final bool isNameClickable;

  const ProfileInkwell(this.child, this.profileUser, this.isNameClickable, {super.key});

  @override
  Widget build(BuildContext context){
    if (isNameClickable){
      return InkWell(
        onTap: () => openProfilePage(context),
        child: child,
      );
    }
    else {
      return child;
    }
  }

  void openProfilePage(BuildContext context){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(profileUser)));
  }
}

class PostHeaderWidget extends StatelessWidget {
  final User user;
  final bool isLeftAligned;
  final bool isNameClickable;
  final bool isCityVisible;
  final bool isBadgeUnder;
  final bool isTopDivider;
  final bool isBottomDivider;
  final List conversation;
  final String? token;
  final User actualUser;

  PostHeaderWidget(this.user, this.isLeftAligned, this.isNameClickable, this.isCityVisible, this.isBadgeUnder, this.isTopDivider, this.isBottomDivider, this.conversation, this.token, this.actualUser, {super.key});

  String userRole = '';
  late List<Widget> nameRowChildren = [
    ProfileInkwell(
      Text(
        '${user.firstName} ${user.name}',
        style: GlobalStyle.interSemiBold
          .copyWith(fontSize: 16)
          .copyWith(color: GlobalStyle.primaryColor),
      ),
      user,
      isNameClickable
    ),
    Visibility(
      visible: !isBadgeUnder,
  // user.type.value
      child: RoleBadge("Particulier", isBadgeUnder),
    ),
  ];

  // variable à laquelle j'ajoute une 2eme row dans les override, à ne pas changer
  late List<Widget> widgetList = [
    Row(
      children: proPostHeaderWidgets,
    ),
  ];

  late Widget additionnalWidget = Container();

  late List<Widget> proPostHeaderWidgets = [
    ProfileInkwell(
      CircleAvatar(
        backgroundImage: AssetImage(user.profilePicFile),
      ),
      user,
      isNameClickable
    ),
    Container(
      margin: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        crossAxisAlignment: isLeftAligned ? CrossAxisAlignment.start : CrossAxisAlignment.end,
        children: [
          Row(
            children: isLeftAligned ? nameRowChildren : nameRowChildren.reversed.toList(),
          ),
          additionnalWidget,
          // Visibility(
            // visible: isCityVisible || isBadgeUnder,
            // child: isCityVisible ?
            // Text(
            //   user.address.city,
            //   style: GlobalStyle.interRegular
            //     .copyWith(fontSize: 15)
            //     .copyWith(color: GlobalStyle.secondaryColor),
            // )
            //     RoleBadge("Particulier", isBadgeUnder),
          // ),
        ],
      ),
    ),
  ];

  late EdgeInsetsGeometry margin = isBadgeUnder ? const EdgeInsets.symmetric(horizontal: 15, vertical: 5) : const EdgeInsets.symmetric(horizontal: 15, vertical: 10);
  late MainAxisAlignment mainRowAlignment = isLeftAligned ? MainAxisAlignment.start : MainAxisAlignment.end;
  late CrossAxisAlignment crossAxisAlignment = isCityVisible ? CrossAxisAlignment.start : CrossAxisAlignment.center;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Column(
        children: [
          _buildDivider(isTopDivider, true),
          Row(
            mainAxisAlignment: mainRowAlignment,
            crossAxisAlignment: crossAxisAlignment,
            children: isLeftAligned ? widgetList : proPostHeaderWidgets.reversed.toList(),
          ),
          _buildDivider(isBottomDivider, false),
        ],
      ),
    );
  }

  Widget _buildDivider(isVisible, isTop){
    return Visibility(
      visible: isVisible,
      child: const Divider(
        thickness: 1,
        color: GlobalStyle.inputGrey,
      ),
    );
  }
}

class UserOfferBannerWidget extends PostHeaderWidget {
  final Post post;
  final Function() refreshFunction;

  UserOfferBannerWidget(super.user, super.isLeftAligned, super.isNameClickable, super.isCityVisible, super.isBadgeUnder, this.post, this.refreshFunction, super.isTopDivider, super.isBottomDivider, super.conversation, super.token, super.actualUser, {super.key});

  @override
  Widget build(BuildContext context){
    super.mainRowAlignment = MainAxisAlignment.spaceBetween;
    super.margin = const EdgeInsets.symmetric(horizontal: 15, vertical: 5);
    super.widgetList.add(
      Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          _offerBtnWidget(true),
          _offerBtnWidget(false),
        ]
      ),
    );
    return super.build(context);
  }

  _offerBtnWidget(bool isValid){
    return GestureDetector(
      onTap: () => _manageOffers(isValid),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 3),
        decoration: BoxDecoration(
          color: isValid ? GlobalStyle.primaryColor : Colors.red,
          borderRadius: BorderRadius.circular(5),
        ),
        child: HeroIcon(
          isValid ? HeroIcons.check : HeroIcons.xMark,
          color: Colors.white,
        ),
      ),
    );
  }

  _manageOffers(bool isValid){
    if (isValid) {
      post.choice = super.user;
      post.offers.clear();
    }
    else {
      post.offers.remove(super.user);
    }

    refreshFunction();

    print(post.offers);
    if (post.choice != null){
      print('choice: ${post.choice!.name}');
    }
    else {
      print('choice: null');
    }
  }
}

class UserProfileBannerWidget extends PostHeaderWidget {
  UserProfileBannerWidget(super.user, super.isLeftAligned, super.isNameClickable, super.isCityVisible, super.isBadgeUnder, super.isTopDivider, super.isBottomDivider, super.conversation, super.token, super.actualUser, {super.key});

  @override
  Widget build(BuildContext context){
    super.mainRowAlignment = MainAxisAlignment.spaceBetween;
    if (super.user.id == super.actualUser.id){
      super.widgetList.add(
        _buildSettingsButton(context),
      );
    } else {
      super.widgetList.add(
        _buildMessageButton(context),
      );
    }

    return super.build(context);
  }

  Widget _buildSettingsButton(BuildContext context){
    return InkWell(
      onTap: () => onBtnPressed(context, SettingsPage()),
      child: Container(
        padding: const EdgeInsets.all(7),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: GlobalStyle.inputGrey),
          borderRadius: BorderRadius.circular(8.0)),
        child: Text(
          "Éditer",
          style: GlobalStyle.interSemiBold
            .copyWith(color: GlobalStyle.secondaryColor)
            .copyWith(fontSize: 14),
        )
      ),
    );
  }

  Widget _buildMessageButton(BuildContext context){
    late Conversation requestedConversation = Conversation(HardData.actualUser, super.user, []);
    return InkWell(
      onTap: () => onBtnPressed(context, null),
      child: Container(
        padding: const EdgeInsets.all(7),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: GlobalStyle.inputGrey),
          borderRadius: BorderRadius.circular(8.0)),
        child: Text(
          "Message",
          style: GlobalStyle.interSemiBold
            .copyWith(color: GlobalStyle.secondaryColor)
            .copyWith(fontSize: 14),
        )
      ),
    );
  }

  onBtnPressed(BuildContext context, Widget? widget){
    Widget newWidget;

    if (widget==null){
      Conversation requestedConversation = Conversation(HardData.actualUser, super.user, []);
      
      for (Conversation conversation in super.conversation){
        if (conversation.creator.id == super.actualUser.id && conversation.otherUser.id == super.user.id ||
        conversation.creator.id == super.user.id && conversation.otherUser.id == super.actualUser.id){
          requestedConversation = conversation;
          print(conversation.messages.length);
        }
        else {
          HardData.allConversations.add(requestedConversation);
          print(requestedConversation.messages.length);
        }
      }
      newWidget = ConversationPage(requestedConversation, super.actualUser, '${super.token}');
    }
    else {
      newWidget = widget;
    }
    print('widget');

    Future.delayed(Duration.zero,() {
      Navigator.push(context, MaterialPageRoute(builder: (context) => newWidget));
    });
  }
}

class ConversationListUserWidget extends PostHeaderWidget {
  String text;
  double textMaxWidth = 0;

  ConversationListUserWidget(super.user, super.isLeftAligned, super.isNameClickable, super.isCityVisible, super.isBadgeUnder, super.isTopDivider, super.isBottomDivider, super.conversation, super.token, super.actualUser, this.text, {super.key});

  @override

  Widget build(BuildContext context){
    if (MediaQuery.of(context).size.width >= 398){
      textMaxWidth = MediaQuery.of(context).size.width * 0.82;
    }
    else {
      textMaxWidth = MediaQuery.of(context).size.width * 0.7;
    }

    super.additionnalWidget = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          constraints : BoxConstraints(maxWidth: textMaxWidth),
          child: Text(
            text,
            maxLines: 1,
            style: GlobalStyle.interRegular
              .copyWith(fontSize: 15)
              .copyWith(color: Colors.black)
          ),
        ),
      ],
    );

    return super.build(context);
  }
}