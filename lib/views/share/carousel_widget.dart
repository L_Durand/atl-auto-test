import 'package:artosage/share/global_style.dart';
import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CarouselWidget extends StatefulWidget {
  final List<Image> _items;
  const CarouselWidget(this._items, {super.key});

  @override
  State<CarouselWidget> createState() => _CarouselWidgetState();
}

class _CarouselWidgetState extends State<CarouselWidget> {
  int pageIndex=0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        CarouselSlider(
          items: widget._items,
          options: CarouselOptions(
            viewportFraction: 1,
            height: 250.0,
            aspectRatio: 1,
            enableInfiniteScroll: false,
            onPageChanged: (index, reason){
              pageIndex=index;
              setState(() {});
            },
          ),
        ),
        Visibility(
          visible: widget._items.length > 1 ? true : false,
          child: Positioned(
          bottom: 20.0,
          child: CarouselIndicator(
            width: 11.0,
            height: 11.0,
            cornerRadius: 10.0,
            count: widget._items.length,
            index: pageIndex,
            color: GlobalStyle.secondaryColor,
          ),
        ),
        ),
      ],
    );
  }
}