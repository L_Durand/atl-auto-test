import 'package:artosage/models/post.dart';
import 'package:artosage/models/user.dart';
import 'package:artosage/service/conversation_service.dart';
import 'package:artosage/service/post_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_scaffold.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/hard_data.dart';
import 'package:artosage/views/share/post_list_widget.dart';
import 'package:flutter/material.dart';

import '../models/conversation.dart';
import '../service/storage_service.dart';

class CivilPostList extends StatefulWidget {
  static String routeName = '/civil_posts';

  // final List<Post> _civilPosts = HardData.civilPosts;
  UserService userService = UserService();
  PostService postService = PostService();
  ConversationService conversationService = ConversationService();
  String? token;

  CivilPostList({super.key});

  @override
  State<CivilPostList> createState() => _CivilPostListState();
}

class _CivilPostListState extends State<CivilPostList> {
  final StorageService storage = StorageService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
        future: getContent(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Si le Future est en attente, afficher une indication de chargement
            return CustomScaffold(
                const Center(
                    child: CircularProgressIndicator(
                        color: GlobalStyle.primaryColor)), true, true, 0, false);
          } else {
            return CustomScaffold(
                PostListWidget(widget.userService.activeUser,
                    widget.postService.posts, widget.conversationService.conversations, widget.token), true, true, 0, false);
          }
        });
  }

  Future<List> getContent() async {
    widget.token = await storage.readSecureData('auth');
    User actualUser = await widget.userService.fetchUser(widget.token);
    List users = await widget.userService.getAllUsers(widget.token);
    List<Post> posts = await widget.postService.getAllPosts(
        widget.token, [PostTypeEnum.ad, PostTypeEnum.adviceRequest], users);
    List<Conversation> conversations = await widget.conversationService.getAllConversations(
        widget.token, users);
    return [actualUser, posts];
  }
}
