import 'package:artosage/service/storage_service.dart';
import 'package:artosage/service/user_service.dart';
import 'package:artosage/share/custom_input.dart';
import 'package:artosage/share/custom_input_label.dart';
import 'package:artosage/share/custom_popup.dart';
import 'package:artosage/share/custom_validation_button.dart';
import 'package:artosage/share/global_style.dart';
import 'package:artosage/share/custom_password_input.dart';
import 'package:artosage/views/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'civil_post_list.dart';
import 'civil_post_page.dart';

class Login extends StatefulWidget {
  static String routeName = '/login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final StorageService storage = StorageService();
  UserService userService = UserService();
  final _formKey = GlobalKey<FormState>();
  var controller;
  var token;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: const Color(0xFF244653),
          image: DecorationImage(
            image: const AssetImage('assets/images/background.png'),
            repeat: ImageRepeat.repeat,
            scale: 3,
            colorFilter: ColorFilter.mode(
              GlobalStyle.secondaryColor.withOpacity(0.8),
              BlendMode.darken,
            ),
          ),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    "A'Rosa-Je",
                    style: GlobalStyle.pacificoRegularWhiteStyle,
                  )),
              CustomInputLabel(text: 'Adresse e-mail'),
              CustomInput(
                  controller: emailController, name: 'une adresse e-mail'),
              CustomInputLabel(text: 'Mot de passe'),
              CustomPasswordInput(controller: passwordController),
              CustomValidationButton(
                text: 'se connecter',
                onPressed: () async {
                  if (!(_formKey.currentState!.validate())) {
                  } else {
                    userService.loginUser(
                        storage,
                        emailController.text.toString(),
                        passwordController.text.toString());
                    String? token = await storage.readSecureData('auth');
                    print(token);
                    if (token != null) {
                      if (token != 'error') {
                        Navigator.pushNamedAndRemoveUntil(
                            context, CivilPostList.routeName, (route) => false);
                      } else {
                        CustomPopup.error(context, 'identifiants invalide');
                      }
                    }
                  }
                },
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: TextButton(
                  onPressed: () async {
                    Navigator.pushNamedAndRemoveUntil(
                        context, Register.routeName, (route) => false);
                  },
                  child: Text(
                    'Pas encore de compte ?',
                    style: GlobalStyle.interSemiBold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
